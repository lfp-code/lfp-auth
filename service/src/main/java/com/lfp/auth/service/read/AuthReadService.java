package com.lfp.auth.service.read;

import java.util.Arrays;
import java.util.stream.Stream;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public interface AuthReadService<K, V> {

	StreamEx<V> get();

	@SuppressWarnings("unchecked")
	default EntryStream<K, V> get(K key, K... keys) {
		Iterable<K> ible = () -> {
			var stream = Arrays.asList(key).stream();
			if (keys != null)
				stream = Stream.concat(stream, Arrays.asList(keys).stream());
			return stream.iterator();
		};
		return get(ible);
	}

	EntryStream<K, V> get(Iterable<? extends K> keyIble);

	static interface Ids<K, V> extends AuthReadService<K, V> {

		StreamEx<K> getIds();
	}

}
