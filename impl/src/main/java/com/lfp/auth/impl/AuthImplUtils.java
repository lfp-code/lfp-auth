package com.lfp.auth.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.auth.service.types.HasIds;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class AuthImplUtils {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int MIN_REQUEST_EXECUTOR_THREADS = 16;
	private static final ThreadLocal<Boolean> REFRESH_POLLLING_THREAD_LOCAL = new ThreadLocal<>();

	public static SubmitterExecutor createRefreshPollingExecutor() {
		var limitedExecutor = Threads.Pools.centralPool()
				.limit(Math.max(MIN_REQUEST_EXECUTOR_THREADS, Utils.Machine.logicalProcessorCount()));
		return SubmitterExecutorAdapter.adaptExecutor(r -> {
			limitedExecutor.submit(() -> {
				REFRESH_POLLLING_THREAD_LOCAL.set(true);
				try {
					r.run();
				} finally {
					REFRESH_POLLLING_THREAD_LOCAL.remove();
				}
			});
		});
	}

	public static boolean isRefreshPollingThread() {
		return Boolean.TRUE.equals(REFRESH_POLLLING_THREAD_LOCAL.get());
	}

	public static <X> EntryStream<X, Boolean> normalizeEntryStream(Iterable<? extends Entry<X, Boolean>> ible) {
		var estream = Streams.of(ible).nonNull().chain(EntryStreams::of);
		estream = estream.nonNullKeys();
		estream = estream.mapValues(Boolean.TRUE::equals);
		estream = estream.map(ent -> normalizeValue(ent)).chain(EntryStreams::of);
		estream = estream.nonNullKeys().nonNullValues();
		return estream;
	}

	public static <X> StreamEx<X> normalizeStream(Iterable<? extends X> ible) {
		return normalizeStream(ible, false);
	}

	public static <X> StreamEx<X> normalizeStream(Iterable<? extends X> ible, boolean distinct) {
		if (ible == null)
			return StreamEx.empty();
		StreamEx<X> stream = Utils.Lots.stream(ible).map(v -> v);
		stream = stream.map(v -> normalizeValue(v)).nonNull();
		if (distinct)
			stream = stream.distinct();
		return stream;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <X> X normalizeValue(X input) {
		if (input == null)
			return null;
		if (input instanceof HasIds) {
			var idStream = ((HasIds) input).streamIds();
			if (idStream == null)
				return null;
			boolean foundId = false;
			for (var id : idStream)
				if (Utils.Strings.isNotBlank(id)) {
					foundId = true;
					break;
				}
			if (!foundId)
				return null;
		}
		if (input instanceof CharSequence) {
			if (Utils.Strings.isBlank((CharSequence) input))
				return null;
		}
		if (input instanceof Entry) {
			var entry = (Entry) input;
			var key = normalizeValue(entry.getKey());
			if (key == null)
				return null;
			var value = normalizeValue(entry.getValue());
			if (value == null)
				return null;
			var entryNorm = Utils.Lots.entry(key, value);
			if (!Objects.equals(entry, entryNorm))
				input = (X) entryNorm;
		}
		if (input instanceof Optional) {
			var op = (Optional) input;
			if (!op.isEmpty()) {
				var value = op.get();
				var valueNorm = normalizeValue(value);
				if (!Objects.equals(value, valueNorm))
					input = (X) Optional.ofNullable(valueNorm);
			}
		}
		return input;
	}

	public static void main(String[] args) {
		var service = new AbstractAuthReadService<Integer, Date>(5, null) {

			@Override
			protected Iterable<? extends Date> getInternal() {
				return StreamEx.empty();
			}

			@Override
			protected Map<Integer, Date> getInternal(List<Integer> keyBatch) {
				Map<Integer, Date> result = new HashMap<>();
				for (var key : keyBatch) {
					if (key == 55) {
						// throw new RuntimeException("fuck");
					}
					Utils.Functions.unchecked(() -> Thread.sleep(Utils.Crypto.getRandomInclusive(100, 2000)));
					result.put(key, new Date());
					System.out.println("finished key:" + key);
				}
				System.out.println("finished map:" + result);
				return result;
			}
		};
		var stream = service.get(IntStreamEx.range(0, 100).mapToObj(v -> v));
		try {
			for (var ent : stream)
				System.out.println(ent);
		} catch (Exception e) {
			logger.warn("error", e);
		}
		System.exit(0);
	}

}
