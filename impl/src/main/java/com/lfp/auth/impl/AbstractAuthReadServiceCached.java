package com.lfp.auth.impl;

import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.google.common.reflect.TypeToken;
import com.lfp.auth.impl.cache.AbstractAuthCache;
import com.lfp.auth.impl.cache.AuthCacheOptions;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingRunnable;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

public abstract class AbstractAuthReadServiceCached<K, V> extends AbstractAuthReadService<K, V> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final int VERSION = 0;
	private static final boolean LOG_POLLING = MachineConfig.isDeveloper() && true;

	private static final Duration POLL_INTERVAL;
	static {
		Duration pollInterval;
		if (MachineConfig.isDeveloper())
			pollInterval = true ? Duration.ofSeconds(5) : null;
		else
			pollInterval = Duration.ofSeconds(5);
		POLL_INTERVAL = pollInterval;
	}

	private final Logger logger;
	private final Map<String, ListenableFuture<Nada>> refreshPollingTaskMap = new ConcurrentHashMap<>();
	private final SubmitterExecutor refreshPollingExecutor = AuthImplUtils.createRefreshPollingExecutor();
	private final Muto<ListenableFuture<Nada>> refreshPollingFutureRef = Muto.create();
	private final AuthCacheOptions<V> valueCacheOptions;
	private final AbstractAuthCache<K, V> valueCache;

	protected AbstractAuthReadServiceCached(int batchSize, boolean concurrent, TypeToken<K> keyTypeToken,
			AuthCacheOptions<V> valueCacheOptions) {
		super(batchSize, concurrent);
		this.valueCacheOptions = Objects.requireNonNull(valueCacheOptions);
		this.valueCache = buildValueCache(valueCacheOptions);
		this.logger = Logging.logger(this.getClass());
	}

	protected AbstractAuthReadServiceCached(int batchSize, Supplier<Executor> requestExecutorSupplier,
			TypeToken<K> keyTypeToken, AuthCacheOptions<V> valueCacheOptions) {
		super(batchSize, requestExecutorSupplier);
		this.valueCacheOptions = Objects.requireNonNull(valueCacheOptions);
		this.valueCache = buildValueCache(valueCacheOptions);
		this.logger = Logging.logger(this.getClass());
	}

	protected AbstractAuthCache<K, V> buildValueCache(AuthCacheOptions<V> valueCacheOptions) {
		return new AbstractAuthCache<K, V>(valueCacheOptions) {

			@Override
			protected V loadFreshValue(@NonNull K key) throws Exception {
				return AbstractAuthReadServiceCached.this.loadFreshValue(key);
			}
		};
	}

	public ListenableFuture<Nada> startRefreshPolling() {
		if (POLL_INTERVAL == null)
			return FutureUtils.immediateResultFuture(Nada.get());
		Supplier<ListenableFuture<Nada>> refreshPollingFutureSupplier = () -> Optional
				.ofNullable(refreshPollingFutureRef.get())
				.filter(Predicate.not(Future::isDone))
				.orElse(null);
		var refreshPollingFuture = refreshPollingFutureSupplier.get();
		if (refreshPollingFuture != null)
			return refreshPollingFuture;
		synchronized (refreshPollingFutureRef) {
			refreshPollingFuture = refreshPollingFutureSupplier.get();
			if (refreshPollingFuture != null)
				return refreshPollingFuture;
			var scheduleRefreshPollTask = new Callable<Nada>() {

				private final AtomicBoolean running = new AtomicBoolean();

				@Override
				public Nada call() throws Exception {
					if (!running.compareAndSet(false, true))
						return Nada.get();
					try {
						scheduleRefreshPollTasks();
					} catch (Throwable t) {
						if (Throws.isHalt(t)) {
							if (t instanceof Exception)
								throw t;
							else
								throw new Exception(t);
						}
						logger.warn("error during refresh polling schedule", t);
					} finally {
						running.set(false);
					}
					return Nada.get();
				}
			};
			if (refreshPollingFutureRef.get() == null)
				// trigger an immediate run
				this.getOnFirstUseNotifier().addListener(nil -> Throws.unchecked(scheduleRefreshPollTask::call));
			refreshPollingFuture = FutureUtils.executeWhile(() -> {
				return Threads.Pools.centralPool().submitScheduled(scheduleRefreshPollTask, POLL_INTERVAL.toMillis());
			}, nil -> true);
			refreshPollingFuture.listener(() -> {
				refreshPollingTaskMap.values().forEach(v -> Threads.Futures.cancel(v, true));
			});
			refreshPollingFutureRef.set(refreshPollingFuture);
			return refreshPollingFuture;
		}
	}

	protected void scheduleRefreshPollTasks() {
		if (LOG_POLLING)
			logger.info("refresh poll started");
		Predicate<String> refreshPollingTaskIdExistsPredicate = key -> this.refreshPollingTaskMap.containsKey(key);
		var idToTaskStream = Utils.Lots.streamEntries(getRefreshPollTasks(refreshPollingTaskIdExistsPredicate))
				.nonNullKeys()
				.nonNullValues();
		List<ListenableFuture<Nada>> startedFutures = LOG_POLLING ? new ArrayList<>() : null;
		for (var ent : idToTaskStream) {
			var id = ent.getKey();
			var task = ent.getValue();
			var futureStarted = Muto.create(false);
			var future = refreshPollingTaskMap.computeIfAbsent(id, nil -> {
				futureStarted.set(true);
				return this.refreshPollingExecutor.submit(() -> {
					runRefreshPollTask(id, task);
					return Nada.get();
				});
			});
			if (!futureStarted.get())
				continue;
			if (startedFutures != null)
				startedFutures.add(future);
			// delay finish to slow down reruns
			future = future.flatMap(delayRefreshPollCompletion());
			future.listener(() -> {
				refreshPollingTaskMap.remove(id);
			});
			Threads.Futures.callbackFailure(future, t -> {
				if (Utils.Exceptions.isCancelException(t))
					return;
				logger.error("error during refresh poll:{}", id, t);
			});
		}
		if (startedFutures != null && startedFutures.size() > 0) {
			logger.info("refresh poll key tasks started. count:{}", startedFutures.size());
			FutureUtils.makeCompleteFuture(startedFutures).listener(() -> {
				var count = 0;
				for (var future : startedFutures)
					if (future.isDone() && !future.isCompletedExceptionally())
						count++;
				logger.info("refresh poll key tasks complete. count:{}", count);
			});
		}
	}

	protected void runRefreshPollTask(String id, ThrowingRunnable<Exception> task) throws Exception {
		var semaphore = new KeepAliveSemaphore(RedissonClients.getDefault(), 1, THIS_CLASS,
				Utils.Lots.stream(this.valueCacheOptions.getStorageKeyParts()).append(id).prepend(VERSION).toArray());
		if (LOG_POLLING)
			semaphore.setVerboseLogging(true);
		var future = semaphore.tryAcquireAsyncKeepAliveSupply(task.asSupplier(), null);
		Threads.Futures.join(future);
	}

	@Override
	protected Map<K, V> getInternal(List<K> keyBatch) {
		Map<K, V> result = new LinkedHashMap<>();
		for (var key : keyBatch) {
			var value = this.valueCache.get(key);
			if (value == null)
				continue;
			result.put(key, value);
		}
		return result;
	}

	protected AbstractAuthCache<K, V> getValueCache() {
		return this.valueCache;
	}

	protected abstract Iterable<? extends Entry<String, ThrowingRunnable<Exception>>> getRefreshPollTasks(
			Predicate<String> refreshPollingTaskIdExistsPredicate);

	protected abstract @Nullable V loadFreshValue(@NonNull K key) throws Exception;

	// utils

	private static <X> Function<X, ListenableFuture<X>> delayRefreshPollCompletion() {
		return result -> {
			return Threads.Pools.centralPool().submitScheduled(() -> result, POLL_INTERVAL.toMillis());
		};
	}

}
