package test;

import com.lfp.auth.auth0.traefik.forward.auth.core.ImmutableForwardAuthProperties;
import com.lfp.joe.serial.Serials;

public class ConfigTest {

	public static void main(String[] args) {
		var cfg = ImmutableForwardAuthProperties.builder()
				.domain("neat.com")
				.tokenEndpoint("a")
				.logoutEndpoint("a")
				.userinfoEndpoint("a")
				.authorizeUrl("a")
				.build();
		System.out.println(Serials.Gsons.getPretty().toJson(cfg));
	}
}
