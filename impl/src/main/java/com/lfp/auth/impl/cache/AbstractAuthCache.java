package com.lfp.auth.impl.cache;

import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.Validate;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.redisson.client.codec.Codec;
import org.slf4j.Logger;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.lfp.auth.impl.AuthImplUtils;
import com.lfp.data.redis.RedisConfig;
import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.data.redisson.tools.cache.RedissonWriterLoader;
import com.lfp.data.tendis.client.config.TendisClientConfig;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.WrapperCache;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

public abstract class AbstractAuthCache<K, V> extends RedissonWriterLoader<K, StatValue<V>>
		implements WrapperCache.Loading<K, K, V, StatValue<V>> {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final int VERSION = 3;

	private static final MemoizedSupplier<RedissonClientLFP> STORAGE_CLIENT_DEFAULT_S = Utils.Functions.memoize(() -> {
		List<Class<? extends RedisConfig>> storageClientConfigTypes = Arrays.asList(TendisClientConfig.class, null);
		Exception toThrow = null;
		for (var storageClientConfigType : storageClientConfigTypes) {
			try {
				RedissonClientLFP client;
				if (storageClientConfigType == null)
					client = RedissonClients.getDefault();
				else {
					RedisConfig cfg = Configs.get(storageClientConfigType);
					Validate.isTrue(Utils.Lots.stream(cfg.tcpConnectionURIs()).nonNull().findFirst().isPresent(),
							"tcpConnectionURIs not found:{}", storageClientConfigType);
					client = RedissonClients.get(cfg);
				}
				Validate.isTrue(client.getBucket(Utils.Crypto.getSecureRandomString()).get() == null,
						"random bucket check failed");
				return client;
			} catch (Exception e) {
				toThrow = e;
			}
		}
		throw toThrow;
	}, null);
	protected final Logger loggerCaller;
	{
		Class<?> loggerClassType = CoreReflections.getCallingClass((i, sf) -> {
			var ct = sf.getDeclaringClass();
			if (ct.isInterface())
				return false;
			if (Modifier.isAbstract(ct.getModifiers()))
				return false;
			if (ct.isAnonymousClass())
				return false;
			return true;
		});
		if (loggerClassType == null)
			loggerClassType = this.getClass();
		loggerCaller = org.slf4j.LoggerFactory.getLogger(loggerClassType);
	}
	private final LoadingCache<K, StatValue<V>> keyValueCache;
	private final AuthCacheOptions<V> options;
	private final GsonCodec gsonCodec;

	public AbstractAuthCache(AuthCacheOptions<V> options) {
		this(STORAGE_CLIENT_DEFAULT_S.get(), options);
	}

	@SuppressWarnings("serial")
	public AbstractAuthCache(RedissonClientLFP storageClient, AuthCacheOptions<V> options) {
		super(storageClient, getCacheKey(options));
		this.options = options;
		this.gsonCodec = new GsonCodec(null,
				new TypeToken<StatValue<V>>() {}.where(new TypeParameter<V>() {}, options.getValueTypeToken()));
		this.keyValueCache = this.buildCache(Caches.newCaffeineBuilder(options.getMaximumSizeMemory(),
				options.getExpireAfterWriteMemory(), options.getExpireAfterAccessMemory()));

	}

	@Override
	protected Codec getBucketCodec() {
		return gsonCodec;
	}

	@Override
	protected @Nullable Iterable<Object> getKeyParts(@NonNull K key) {
		return Arrays.asList(key);
	}

	@Override
	protected @Nullable ThrowingFunction<ThrowingSupplier<StatValue<V>, Exception>, StatValue<V>, Exception> getStorageLockAccessor(
			@NonNull K key, String lockKey) {
		var semaphore = new KeepAliveSemaphore(RedissonClients.getDefault(), 1, lockKey);
		return loader -> {
			var statValueFuture = semaphore.acquireAsyncKeepAliveSupply(loader);
			var completion = Completion.get(statValueFuture);
			if (completion.isFailure())
				throw CoreReflections.tryCast(completion.failure(), Exception.class)
						.orElseGet(() -> new Exception(completion.failure()));
			return completion.result();
		};
	}

	@Override
	protected @Nullable Duration getStorageTTL(@NonNull K key, @NonNull StatValue<V> result) {
		if (result == null)
			return Duration.ZERO;
		Duration maxTTL;
		if (result.getValue() == null)
			maxTTL = this.options.getExpireAfterWriteStorageNull();
		else
			maxTTL = this.options.getExpireAfterWriteStorage();
		if (maxTTL == null || maxTTL.toMillis() <= 0)
			return Duration.ZERO;
		long elapsedMillis = result.getElapsedSinceCreate().toMillis();
		var ttlMillis = maxTTL.toMillis() - elapsedMillis;
		return Duration.ofMillis(ttlMillis);
	}

	@Override
	public StatValue<V> storageUpdate(@NonNull K key, @Nullable StatValue<V> value) throws Exception {
		return super.storageUpdate(key, value);
	}

	@Override
	public @Nullable StatValue<V> storageGet(@NonNull K key) throws Exception {
		return super.storageGet(key);
	}

	@Override
	protected @Nullable StatValue<V> loadFresh(@NonNull K key) throws Exception {
		var value = loadFreshValue(key);
		if (value == null) {
			var expireAfterWriteStorageNull = this.options.getExpireAfterWriteStorageNull();
			if (expireAfterWriteStorageNull == null || expireAfterWriteStorageNull.toMillis() <= 0)
				return null;
		}
		return StatValue.build(value);
	}

	@Override
	public K wrapKey(K key) {
		return key;
	}

	@Override
	public K unwrapKey(K wrappedKey) {
		return wrappedKey;
	}

	@Override
	public StatValue<V> wrapValue(V value) {
		return StatValue.build(value);
	}

	@Override
	public V unwrapValue(StatValue<V> wrappedValue) {
		return wrappedValue == null ? null : wrappedValue.getValue();
	}

	@Override
	public LoadingCache<K, StatValue<V>> getDelegate() {
		return this.keyValueCache;
	}

	protected String getCacheKey(Iterable<Object> append) {
		return getCacheKey(Utils.Lots.stream(append).toArray());
	}

	protected String getCacheKey(Object... append) {
		return getCacheKey(this.options, append);
	}

	private static String getCacheKey(AuthCacheOptions<?> authCacheOptions, Object... append) {
		Objects.requireNonNull(authCacheOptions);
		var stream = Utils.Lots.stream(authCacheOptions.getStorageKeyParts());
		stream = stream.append(Utils.Lots.stream(append));
		stream = AuthImplUtils.normalizeStream(stream);
		stream = Utils.Lots.requireNonEmpty(stream);
		stream = stream.prepend(THIS_CLASS, VERSION);
		var result = RedisUtils.generateKey(stream.toArray());
		return result;
	}

	protected abstract V loadFreshValue(@NonNull K key) throws Exception;

}
