package com.lfp.auth.undertow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Deque;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.auth.undertow.config.AuthUndertowConfig;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.util.HeaderValues;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class JwtCandidateReader implements Function<HttpServerExchange, StreamEx<String>> {

	public static final JwtCandidateReader INSTANCE;
	static {
		var cfg = Configs.get(AuthUndertowConfig.class);
		INSTANCE = new JwtCandidateReader(cfg.tokenHeaderNames(), cfg.tokenCookieNames(), cfg.tokenQueryParamNames());
	}
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final List<String> headerNames;
	private final List<String> cookieNames;
	private final List<String> queryParamNames;

	public JwtCandidateReader(Iterable<String> headerNames, Iterable<String> cookieNames,
			Iterable<String> queryParameterNames) {
		this.headerNames = Utils.Lots.stream(headerNames).mapPartial(Utils.Strings::trimToNullOptional)
				.toImmutableList();
		this.cookieNames = Utils.Lots.stream(cookieNames).mapPartial(Utils.Strings::trimToNullOptional)
				.toImmutableList();
		this.queryParamNames = Utils.Lots.stream(queryParameterNames).mapPartial(Utils.Strings::trimToNullOptional)
				.toImmutableList();

	}

	@Override
	public StreamEx<String> apply(HttpServerExchange hse) {
		if (hse == null)
			return StreamEx.empty();
		var sw = MachineConfig.isDeveloper() ? StopWatch.createStarted() : null;
		StreamEx<Object> candidateObjectStream = StreamEx.empty();
		{
			var append = Utils.Lots.defer(() -> streamHeaderValues(hse));
			candidateObjectStream = candidateObjectStream.append(append);
		}
		{
			var append = Utils.Lots.defer(() -> streamCookieValues(hse));
			candidateObjectStream = candidateObjectStream.append(append);
		}
		{
			var append = Utils.Lots.defer(() -> streamQueryParameterValues(hse));
			candidateObjectStream = candidateObjectStream.append(append);
		}
		candidateObjectStream = Utils.Lots.flatMapAll(candidateObjectStream).nonNull();
		var candidateStream = candidateObjectStream.mapPartial(Utils.Strings::toStringOptional)
				.mapPartial(Utils.Strings::trimToNullOptional);
		if (sw != null) {
			candidateStream = Utils.Lots.stream(candidateStream.toList());
			logger.info("read time:{}", sw.getTime());
		}
		return candidateStream;
	}

	private StreamEx<HeaderValues> streamHeaderValues(HttpServerExchange hse) {
		if (this.headerNames.isEmpty())
			return StreamEx.empty();
		var reqHeaders = hse.getRequestHeaders();
		if (reqHeaders == null)
			return StreamEx.empty();
		var headerNameStream = streamFiltered(this.headerNames, reqHeaders.getHeaderNames(), Object::toString);
		var result = headerNameStream.map(v -> reqHeaders.get(v));
		return result;
	}

	private StreamEx<String> streamCookieValues(HttpServerExchange hse) {
		if (this.cookieNames.isEmpty())
			return StreamEx.empty();
		var requestCookies = hse.requestCookies();
		if (requestCookies == null)
			return StreamEx.empty();
		var cookieStream = streamFiltered(this.cookieNames, requestCookies, Cookie::getName);
		cookieStream = cookieStream.filter(v -> {
			var expires = v.getExpires();
			if (expires != null && !expires.before(new Date()))
				return false;
			if (v.isDiscard())
				return false;
			return true;
		});
		var result = cookieStream.map(v -> v.getValue());
		return result;
	}

	private StreamEx<Deque<String>> streamQueryParameterValues(HttpServerExchange hse) {
		if (this.queryParamNames.isEmpty())
			return StreamEx.empty();
		var queryParameters = hse.getQueryParameters();
		if (queryParameters == null)
			return StreamEx.empty();
		var result = streamFiltered(this.queryParamNames, queryParameters.entrySet(), e -> e.getKey())
				.map(Entry::getValue);
		return result;
	}

	private static <X> StreamEx<X> streamFiltered(List<String> filters, Iterable<? extends X> input,
			Function<X, String> toString) {
		StreamEx<X> stream = Utils.Lots.stream(input).map(v -> v);
		stream = stream.nonNull();
		if (filters == null || filters.isEmpty() || toString == null)
			return stream;
		stream = stream.filter(v -> {
			var str = toString.apply(v);
			if (str == null)
				return false;
			for (var filter : filters)
				if (Utils.Strings.equalsIgnoreCase(filter, str))
					return true;
			return false;
		});
		return stream;
	}

	public static void main(String[] args) {
		List<Object> list = new ArrayList<>();
		list.add(Arrays.asList("cool", IntStreamEx.range(5), null));
		list.add("fun");
		list.add("  ");
		list.add("alright");
		var candidateObjectStream = Utils.Lots.stream(list);
		candidateObjectStream = Utils.Lots.flatMapAll(candidateObjectStream).nonNull();
		var candidateStream = candidateObjectStream.mapPartial(Utils.Strings::toStringOptional)
				.mapPartial(Utils.Strings::trimToNullOptional);
		candidateStream.forEach(v -> {
			System.out.println(v);
		});
	}

}
