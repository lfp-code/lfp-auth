package com.lfp.auth.service.types;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.google.common.collect.ImmutableList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.beans.joda.JodaBeans;

@BeanDefinition
public class Group extends HasIds.Abs implements ImmutableBean {

	@SerializedName(value = "_id", alternate = "id")
	@PropertyDefinition(get = "protected")
	@Expose
	private final String id;
	@SerializedName(value = "group_id", alternate = "groupId")
	@PropertyDefinition(get = "protected")
	@Expose
	private final String groupId;
	@PropertyDefinition
	@Expose
	private final String name;
	@PropertyDefinition
	@Expose
	private final String description;
	@PropertyDefinition
	@Expose
	private final List<String> nested;
	@PropertyDefinition
	@Expose
	private final List<String> members;
	@PropertyDefinition
	@SerializedName("group_metadata")
	@Expose
	private final Metadata groupMetadata;

	@Override
	protected Iterable<String> getIdsInternal() {
		return Arrays.asList(this.id, this.groupId);
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code Group}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static Group.Meta meta() {
		return Group.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(Group.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static Group.Builder builder() {
		return new Group.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected Group(Group.Builder builder) {
		this.id = builder.id;
		this.groupId = builder.groupId;
		this.name = builder.name;
		this.description = builder.description;
		this.nested = (builder.nested != null ? ImmutableList.copyOf(builder.nested) : null);
		this.members = (builder.members != null ? ImmutableList.copyOf(builder.members) : null);
		this.groupMetadata = builder.groupMetadata;
	}

	@Override
	public Group.Meta metaBean() {
		return Group.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the id.
	 * 
	 * @return the value of the property
	 */
	protected String getId() {
		return id;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the groupId.
	 * 
	 * @return the value of the property
	 */
	protected String getGroupId() {
		return groupId;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the name.
	 * 
	 * @return the value of the property
	 */
	public String getName() {
		return name;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the description.
	 * 
	 * @return the value of the property
	 */
	public String getDescription() {
		return description;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the nested.
	 * 
	 * @return the value of the property
	 */
	public List<String> getNested() {
		return nested;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the members.
	 * 
	 * @return the value of the property
	 */
	public List<String> getMembers() {
		return members;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the groupMetadata.
	 * 
	 * @return the value of the property
	 */
	public Metadata getGroupMetadata() {
		return groupMetadata;
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	public Builder toBuilder() {
		return new Builder(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			Group other = (Group) obj;
			return JodaBeanUtils.equal(id, other.id) && JodaBeanUtils.equal(groupId, other.groupId)
					&& JodaBeanUtils.equal(name, other.name) && JodaBeanUtils.equal(description, other.description)
					&& JodaBeanUtils.equal(nested, other.nested) && JodaBeanUtils.equal(members, other.members)
					&& JodaBeanUtils.equal(groupMetadata, other.groupMetadata);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + JodaBeanUtils.hashCode(id);
		hash = hash * 31 + JodaBeanUtils.hashCode(groupId);
		hash = hash * 31 + JodaBeanUtils.hashCode(name);
		hash = hash * 31 + JodaBeanUtils.hashCode(description);
		hash = hash * 31 + JodaBeanUtils.hashCode(nested);
		hash = hash * 31 + JodaBeanUtils.hashCode(members);
		hash = hash * 31 + JodaBeanUtils.hashCode(groupMetadata);
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(256);
		buf.append("Group{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	protected void toString(StringBuilder buf) {
		buf.append("id").append('=').append(JodaBeanUtils.toString(id)).append(',').append(' ');
		buf.append("groupId").append('=').append(JodaBeanUtils.toString(groupId)).append(',').append(' ');
		buf.append("name").append('=').append(JodaBeanUtils.toString(name)).append(',').append(' ');
		buf.append("description").append('=').append(JodaBeanUtils.toString(description)).append(',').append(' ');
		buf.append("nested").append('=').append(JodaBeanUtils.toString(nested)).append(',').append(' ');
		buf.append("members").append('=').append(JodaBeanUtils.toString(members)).append(',').append(' ');
		buf.append("groupMetadata").append('=').append(JodaBeanUtils.toString(groupMetadata)).append(',').append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code Group}.
	 */
	public static class Meta extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code id} property.
		 */
		private final MetaProperty<String> id = DirectMetaProperty.ofImmutable(this, "id", Group.class, String.class);
		/**
		 * The meta-property for the {@code groupId} property.
		 */
		private final MetaProperty<String> groupId = DirectMetaProperty.ofImmutable(this, "groupId", Group.class,
				String.class);
		/**
		 * The meta-property for the {@code name} property.
		 */
		private final MetaProperty<String> name = DirectMetaProperty.ofImmutable(this, "name", Group.class,
				String.class);
		/**
		 * The meta-property for the {@code description} property.
		 */
		private final MetaProperty<String> description = DirectMetaProperty.ofImmutable(this, "description",
				Group.class, String.class);
		/**
		 * The meta-property for the {@code nested} property.
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private final MetaProperty<List<String>> nested = DirectMetaProperty.ofImmutable(this, "nested", Group.class,
				(Class) List.class);
		/**
		 * The meta-property for the {@code members} property.
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private final MetaProperty<List<String>> members = DirectMetaProperty.ofImmutable(this, "members", Group.class,
				(Class) List.class);
		/**
		 * The meta-property for the {@code groupMetadata} property.
		 */
		private final MetaProperty<Metadata> groupMetadata = DirectMetaProperty.ofImmutable(this, "groupMetadata",
				Group.class, Metadata.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null, "id",
				"groupId", "name", "description", "nested", "members", "groupMetadata");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				return id;
			case 293428218: // groupId
				return groupId;
			case 3373707: // name
				return name;
			case -1724546052: // description
				return description;
			case -1048944393: // nested
				return nested;
			case 948881689: // members
				return members;
			case 1602610894: // groupMetadata
				return groupMetadata;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public Group.Builder builder() {
			return new Group.Builder();
		}

		@Override
		public Class<? extends Group> beanType() {
			return Group.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code id} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> id() {
			return id;
		}

		/**
		 * The meta-property for the {@code groupId} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> groupId() {
			return groupId;
		}

		/**
		 * The meta-property for the {@code name} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> name() {
			return name;
		}

		/**
		 * The meta-property for the {@code description} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> description() {
			return description;
		}

		/**
		 * The meta-property for the {@code nested} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<List<String>> nested() {
			return nested;
		}

		/**
		 * The meta-property for the {@code members} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<List<String>> members() {
			return members;
		}

		/**
		 * The meta-property for the {@code groupMetadata} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Metadata> groupMetadata() {
			return groupMetadata;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				return ((Group) bean).getId();
			case 293428218: // groupId
				return ((Group) bean).getGroupId();
			case 3373707: // name
				return ((Group) bean).getName();
			case -1724546052: // description
				return ((Group) bean).getDescription();
			case -1048944393: // nested
				return ((Group) bean).getNested();
			case 948881689: // members
				return ((Group) bean).getMembers();
			case 1602610894: // groupMetadata
				return ((Group) bean).getGroupMetadata();
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code Group}.
	 */
	public static class Builder extends DirectFieldsBeanBuilder<Group> {

		private String id;
		private String groupId;
		private String name;
		private String description;
		private List<String> nested;
		private List<String> members;
		private Metadata groupMetadata;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(Group beanToCopy) {
			this.id = beanToCopy.getId();
			this.groupId = beanToCopy.getGroupId();
			this.name = beanToCopy.getName();
			this.description = beanToCopy.getDescription();
			this.nested = (beanToCopy.getNested() != null ? ImmutableList.copyOf(beanToCopy.getNested()) : null);
			this.members = (beanToCopy.getMembers() != null ? ImmutableList.copyOf(beanToCopy.getMembers()) : null);
			this.groupMetadata = beanToCopy.getGroupMetadata();
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				return id;
			case 293428218: // groupId
				return groupId;
			case 3373707: // name
				return name;
			case -1724546052: // description
				return description;
			case -1048944393: // nested
				return nested;
			case 948881689: // members
				return members;
			case 1602610894: // groupMetadata
				return groupMetadata;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				this.id = (String) newValue;
				break;
			case 293428218: // groupId
				this.groupId = (String) newValue;
				break;
			case 3373707: // name
				this.name = (String) newValue;
				break;
			case -1724546052: // description
				this.description = (String) newValue;
				break;
			case -1048944393: // nested
				this.nested = (List<String>) newValue;
				break;
			case 948881689: // members
				this.members = (List<String>) newValue;
				break;
			case 1602610894: // groupMetadata
				this.groupMetadata = (Metadata) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public Group build() {
			return new Group(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the id.
		 * 
		 * @param id the new value
		 * @return this, for chaining, not null
		 */
		public Builder id(String id) {
			this.id = id;
			return this;
		}

		/**
		 * Sets the groupId.
		 * 
		 * @param groupId the new value
		 * @return this, for chaining, not null
		 */
		public Builder groupId(String groupId) {
			this.groupId = groupId;
			return this;
		}

		/**
		 * Sets the name.
		 * 
		 * @param name the new value
		 * @return this, for chaining, not null
		 */
		public Builder name(String name) {
			this.name = name;
			return this;
		}

		/**
		 * Sets the description.
		 * 
		 * @param description the new value
		 * @return this, for chaining, not null
		 */
		public Builder description(String description) {
			this.description = description;
			return this;
		}

		/**
		 * Sets the nested.
		 * 
		 * @param nested the new value
		 * @return this, for chaining, not null
		 */
		public Builder nested(List<String> nested) {
			this.nested = nested;
			return this;
		}

		/**
		 * Sets the {@code nested} property in the builder from an array of objects.
		 * 
		 * @param nested the new value
		 * @return this, for chaining, not null
		 */
		public Builder nested(String... nested) {
			return nested(ImmutableList.copyOf(nested));
		}

		/**
		 * Sets the members.
		 * 
		 * @param members the new value
		 * @return this, for chaining, not null
		 */
		public Builder members(List<String> members) {
			this.members = members;
			return this;
		}

		/**
		 * Sets the {@code members} property in the builder from an array of objects.
		 * 
		 * @param members the new value
		 * @return this, for chaining, not null
		 */
		public Builder members(String... members) {
			return members(ImmutableList.copyOf(members));
		}

		/**
		 * Sets the groupMetadata.
		 * 
		 * @param groupMetadata the new value
		 * @return this, for chaining, not null
		 */
		public Builder groupMetadata(Metadata groupMetadata) {
			this.groupMetadata = groupMetadata;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(256);
			buf.append("Group.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("id").append('=').append(JodaBeanUtils.toString(id)).append(',').append(' ');
			buf.append("groupId").append('=').append(JodaBeanUtils.toString(groupId)).append(',').append(' ');
			buf.append("name").append('=').append(JodaBeanUtils.toString(name)).append(',').append(' ');
			buf.append("description").append('=').append(JodaBeanUtils.toString(description)).append(',').append(' ');
			buf.append("nested").append('=').append(JodaBeanUtils.toString(nested)).append(',').append(' ');
			buf.append("members").append('=').append(JodaBeanUtils.toString(members)).append(',').append(' ');
			buf.append("groupMetadata").append('=').append(JodaBeanUtils.toString(groupMetadata)).append(',')
					.append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
