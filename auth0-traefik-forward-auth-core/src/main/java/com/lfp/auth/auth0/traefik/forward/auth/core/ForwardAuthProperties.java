package com.lfp.auth.auth0.traefik.forward.auth.core;

import java.util.List;

import org.aeonbits.owner.Config;
import org.apache.commons.lang3.Validate;
import org.immutables.gson.Gson;
import org.immutables.value.Value;

import com.github.throwable.beanref.BeanPath;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

@Gson.TypeAdapters
@Value.Immutable
public interface ForwardAuthProperties extends Config {

	@Key("domain")
	String getDomain();

	@Key("tokenEndpoint")
	String getTokenEndpoint();

	@Key("logoutEndpoint")
	String getLogoutEndpoint();

	@Key("userinfoEndpoint")
	String getUserinfoEndpoint();

	@Key("authorizeUrl")
	String getAuthorizeUrl();

	@Value.Default
	default int getNonceMaxAge() {
		return 60;
	}

	@Value.Check
	default ForwardAuthProperties validate() {
		Validate.isTrue(getNonceMaxAge() >= -1, "nonceMaxAge required - %s", this);
		Const.CHAR_SEQUENCE_BEAN_PATHS.stream()
				.forEach(beanPath -> {
					var value = beanPath.get(this);
					Validate.notBlank(value, "%s required - %s", beanPath.getPath(), this);
				});
		return this;
	}

	static enum Const {
		;

		private static final List<BeanPath<ForwardAuthProperties, ? extends CharSequence>> CHAR_SEQUENCE_BEAN_PATHS = ForwardAuthConfigs
				.charSequenceBeanPaths(ForwardAuthProperties.class)
				.toImmutableList();
	}

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
	}
}
