package com.lfp.auth.service.types;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.BaseStream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public interface HasIds extends Hashable {

	StreamEx<String> streamIds();

	@Override
	default Bytes hash() {
		return HasIds.hash(this);
	}

	public abstract static class Abs implements HasIds {

		private transient Bytes _hash;

		@Override
		public Bytes hash() {
			if (_hash == null)
				_hash = HasIds.hash(this);
			return _hash;
		}

		@Override
		public StreamEx<String> streamIds() {
			return HasIds.streamIds(getIdsInternal());
		}

		@Override
		public int hashCode() {
			return Objects.hash(hash());
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof HasIds))
				return false;
			if (this == obj)
				return false;
			var hash = this.hash();
			return hash.equals(((HasIds) obj).hash());
		}

		protected abstract Iterable<String> getIdsInternal();

	}

	// utils

	public static <X extends HasIds> StreamEx<String> streamIds(X bean) {
		if (bean == null)
			return StreamEx.empty();
		return streamIds(bean.streamIds());
	}

	public static StreamEx<String> streamIds(Iterable<? extends CharSequence> ids) {
		StreamEx<String> stream = Streams.of(ids).nonNull().map(Object::toString);
		stream = stream.filter(Utils.Strings::isNotBlank);
		stream = stream.distinct();
		return stream;
	}

	public static <X extends HasIds> EntryStream<String, X> streamEntries(Iterable<? extends X> ible) {
		StreamEx<X> inputStream = Streams.of(ible);
		inputStream = inputStream.nonNull();
		EntryStream<String, X> idToBeanStream = inputStream.map(v -> {
			return streamIds(v).mapToEntry(nil -> v);
		}).chain(Streams.flatMap()).chain(EntryStreams::of);
		return idToBeanStream;
	}

	public static <X extends HasIds> Function<BaseStream<? extends X, ?>, StreamEx<X>> distinctNonNull() {
		return stream -> {
			if (stream == null)
				return Streams.empty();
			StreamEx<X> resultStream = Streams.of(stream).map(v -> (X) v).nonNull();
			Set<String> distinctTracker = resultStream.isParallel()
					? Collections.newSetFromMap(new ConcurrentHashMap<>())
					: new HashSet<>();
			return resultStream.filter(v -> {
				var mod = false;
				for (var id : v.streamIds())
					if (distinctTracker.add(id))
						mod = true;
				return mod;
			});

		};
	}

	private static Bytes hash(HasIds hasIds) {
		Objects.requireNonNull(hasIds);
		var classType = CoreReflections.getNamedClassType(hasIds.getClass());
		StreamEx<Object> partStream = hasIds.streamIds().map(v -> (Object) v);
		partStream = partStream.distinct();
		partStream = partStream.sorted();
		partStream = partStream.prepend(classType);
		return Utils.Crypto.hashMD5(partStream.toArray());
	}

}
