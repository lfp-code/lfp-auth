package com.lfp.auth.auth0.traefik.forward.auth.core;

import java.util.Objects;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodsRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.classpath.MemberCache.Invoker;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

@SuppressWarnings("unchecked")
public class ForwardAuthConfigs {

	protected ForwardAuthConfigs() {}

	private static final Class<?> STRING_ARRAY_CLASS_TYPE = EmptyArrays.of(String.class).getClass();

	public static <U> Invoker<U, U> invoker(BeanPath<U, ?> beanPath) {
		var methodName = "with" + Utils.Strings.capitalize(beanPath.getPath());
		var method = MemberCache.tryGetMethod(
				MethodRequest.of(beanPath.getBeanClass(), beanPath.getBeanClass(), methodName, beanPath.getType()),
				true).orElseGet(() -> {
					var methods = MemberCache.getMethods(MethodsRequest.of(beanPath.getBeanClass()));
					methods = Streams.of(methods)
							.filter(v -> methodName.equalsIgnoreCase(v.getName()))
							.limit(2)
							.toList();
					if (methods.size() == 1)
						return methods.get(0);
					return null;
				});
		Objects.requireNonNull(method, () -> "invoker not found" + beanPath);
		return (b, args) -> (U) Throws.unchecked(() -> method.invoke(b, args));
	}

	public static <U> StreamEx<BeanPath<U, ? extends CharSequence>> charSequenceBeanPaths(Class<U> classType) {
		var bps = BeanRef.$(classType).all();
		return Streams.of(bps)
				.filter(v -> CharSequence.class.isAssignableFrom(v.getType()))
				.map(v -> (BeanPath<U, ? extends CharSequence>) v);
	}

	public static <U> StreamEx<BeanPath<U, String[]>> stringArrayBeanPaths(Class<U> classType) {
		var bps = BeanRef.$(classType).all();
		return Streams.of(bps)
				.filter(v -> STRING_ARRAY_CLASS_TYPE.isAssignableFrom(v.getType()))
				.map(v -> (BeanPath<U, String[]>) v);
	}
}
