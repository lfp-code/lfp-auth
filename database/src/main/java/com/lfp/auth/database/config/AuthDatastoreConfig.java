package com.lfp.auth.database.config;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

public interface AuthDatastoreConfig extends Config {

	@DefaultValue("auth_v1")
	String databaseName();

	// not really a uri, but they call it one
	String connectionURI();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.json());
	}
}
