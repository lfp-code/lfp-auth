package com.lfp.auth.service.config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.net.http.oauth.OauthServiceConfig;

public interface AuthServiceConfig extends OauthServiceConfig {

	public static final String PATH_BASE = "/lfp/auth";

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.json());
	}
}
