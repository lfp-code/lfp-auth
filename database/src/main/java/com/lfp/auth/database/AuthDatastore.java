package com.lfp.auth.database;

import com.lfp.auth.database.config.AuthDatastoreConfig;
import com.lfp.data.mongo.morphia.MapperLFP;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.stream.Streams;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import dev.morphia.DatastoreImpl;
import dev.morphia.Morphia;

@SuppressWarnings("deprecation")
public class AuthDatastore extends DatastoreImpl {

	public static AuthDatastore get() {
		return Instances.get(AuthDatastore.class, () -> {
			MongoClientURI uri = new MongoClientURI(Configs.get(AuthDatastoreConfig.class).connectionURI());
			MongoClient mongoClient = new MongoClient(uri);
			return new AuthDatastore(new MapperLFP(), mongoClient);
		});
	}

	protected AuthDatastore(MapperLFP mapper, MongoClient mongoClient) {
		super(new Morphia(mapper), mapper, mongoClient, Configs.get(AuthDatastoreConfig.class).databaseName());
		}

	public static void main(String[] args) {
		var collectionNames = AuthDatastore.get().getDatabase().listCollectionNames();
		System.out.println(Streams.of(collectionNames).toList());
	}
}