package com.lfp.auth.auth0.management.config;

import java.net.URI;
import java.time.Duration;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.net.http.oauth.OauthServiceConfig;
import com.lfp.joe.properties.converter.DurationConverter;

public interface Auth0ManagementConfig extends OauthServiceConfig {

	String tenant();

	@Override
	String clientId();

	@DefaultValue("client_id")
	String clientIdKey();

	@Override
	String clientSecret();

	@DefaultValue("client_secret")
	String clientSecretKey();

	@Override
	@DefaultValue("https://${tenant}.auth0.com")
	@ConverterClass(URIConverter.class)
	URI uri();

	@Override
	@DefaultValue("${uri}/oauth/token")
	@ConverterClass(URIConverter.class)
	URI oauthURI();

	@Override
	@DefaultValue("client_credentials")
	String grantType();

	@Override
	@DefaultValue("")
	String scope();

	@DefaultValue("/api/v2")
	String apiV2Path();

	@DefaultValue("60") // https://auth0.com/docs/troubleshoot/customer-support/operational-policies/rate-limit-policy/management-api-endpoint-rate-limits
	long rateLimiterRate();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("5 second")
	Duration rateLimiterInterval();

	public static void main(String[] args) {
		Configs.printProperties(
				PrintOptions.propertiesBuilder().withIncludeValues(true).withSkipPopulated(false).build());
		Configs.printProperties(PrintOptions.jsonBuilder().withSkipPopulated(false).build());
	}
}
