package test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import org.apache.commons.lang3.time.StopWatch;

import com.github.benmanes.caffeine.cache.Cache;
import org.apache.commons.lang3.Validate;
import com.google.gson.Gson;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.jwt.resolver.HashedSigningKeyResolver;
import com.lfp.joe.jwt.resolver.IssuerClaimSigningKeyResolver;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.socket.socks.Sockets;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.retry.Retrys;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.RSAKeyPair;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.ECKeyGenerator;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import at.favre.lib.bytes.Bytes;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;

public class JWTTest {

	public static void main(String[] args) {
		try {
			run();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}

	public static void run() throws JOSEException, ParseException, NoSuchAlgorithmException, IOException {
		var ent = generatePublicKeyAndKey();
		var jwkSetPublicJson = ent.getKey();
		var jwtEncoded = ent.getValue();
		URI uri = URI.create("http://localhost:" + Sockets.allocatePort());
		HttpHandler handler = hse -> {
			hse.setStatusCode(StatusCodes.OK);
			hse.startBlocking();
			try (var os = hse.getOutputStream()) {
				Utils.Bits.copy(Utils.Bits.from(jwkSetPublicJson).inputStream(), os);
			} finally {
				hse.endExchange();
			}
		};
		handler = new ThreadHttpHandler(handler);
		Undertow server = Undertows.serverBuilder(uri.getPort()).setHandler(handler).build();
		server.start();
		System.out.println(uri);
		Retrys.execute(Integer.MAX_VALUE, Duration.ofMillis(500), nil -> {
			var response = HttpRequests.request().uri(uri).send();
			try (var is = response.body()) {
				Utils.Bits.copy(is, OutputStream.nullOutputStream());
				StatusCodes.validate(response.statusCode(), 2);
			}
			return Nada.get();
		});

		var signingKeyResolverDelegate = IssuerClaimSigningKeyResolver.getDefault(uri.getHost());
		var signingKeyResolver = new HashedSigningKeyResolver() {

			private final Cache<Bytes, Optional<Key>> keyCache = Caches.newCaffeineBuilder(-1, null, null).build();

			@Override
			protected Key resolveSigningKey(JwsHeader<?> header, Claims claims, Supplier<Bytes> hashSupplier) {
				var hashSw = StopWatch.createStarted();
				var hash = hashSupplier.get();
				System.out.println(hash.encodeHex());
				System.out.println(hashSw.getTime());
				return keyCache.get(hash, nil -> {
					System.out.println("loading key:" + hash.encodeHex());
					var key = signingKeyResolverDelegate.resolveSigningKey(header, claims);
					return Optional.of(key);
				}).orElse(null);
			}
		};
		JwtParser parser = Jwts.parserBuilder().setSigningKeyResolver(signingKeyResolver).build();
		for (int i = 0; i < 5; i++) {
			StopWatch sw = new StopWatch();
			sw.start();
			Jwt parsed = parser.parse(jwtEncoded);
			System.out.println(sw.getTime());
			System.out.println(parsed);
		}
	}

	@SuppressWarnings("unused")
	private static Entry<String, String> generatePublicKeyAndKey() throws JOSEException {
		JWK publicJWK;
		JWSSigner signer;
		String jwkSetPublicJson;
		if (true) {
			var algorithm = JWSAlgorithm.PS512;
			RSAKey rsaJWK;
			if (true) {
				RSAKeyPair keyPair = Utils.Crypto.generateRSAKeyPair();
				StopWatch sw = StopWatch.createStarted();
				System.out.println(sw.getTime());
				RSAKey.Builder builder = new RSAKey.Builder(keyPair.getPublicKey()).privateKey(keyPair.getPrivateKey())
						.keyID("123").algorithm(algorithm);
				rsaJWK = builder.build();
			} else {
				rsaJWK = new RSAKeyGenerator(4096).keyID("123").algorithm(algorithm).generate();
			}
			publicJWK = rsaJWK.toPublicJWK();
			// Create the EdDSA signer
			signer = new RSASSASigner(rsaJWK);
			var jwkSet = new JWKSet(rsaJWK);
			System.out.println(new Gson().toJson(jwkSet.toJSONObject(false)));
			jwkSetPublicJson = new Gson().toJson(jwkSet.toPublicJWKSet().toJSONObject());
		} else {
			StopWatch sw = StopWatch.createStarted();
			ECKey jwk = new ECKeyGenerator(Curve.P_521).keyUse(KeyUse.SIGNATURE).keyID("123")
					.algorithm(JWSAlgorithm.ES512).generate();
			System.out.println(sw.getTime());
			publicJWK = jwk.toPublicJWK();
			// Create the EdDSA signer
			signer = new ECDSASigner(jwk);
			var jwkSet = new JWKSet(jwk);
			System.out.println(new Gson().toJson(jwkSet.toJSONObject(false)));
			jwkSetPublicJson = new Gson().toJson(jwkSet.toPublicJWKSet().toJSONObject());
		}
		System.out.println(jwkSetPublicJson);
		// Prepare JWT with claims set
		var claimsSetBuilder = new JWTClaimsSet.Builder().subject("alice").issuer("https://c2id.com")
				.expirationTime(new Date(new Date().getTime() + Duration.ofDays(1).toMillis()));
		for (int i = 0; i < 25; i++)
			claimsSetBuilder = claimsSetBuilder.claim("claim_" + i, Map.of(1, "cool", 2, new Date()));
		StopWatch sw = StopWatch.createStarted();
		SignedJWT signedJWT = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) publicJWK.getAlgorithm()).keyID(publicJWK.getKeyID()).build(),
				claimsSetBuilder.build());
		signedJWT.sign(signer);
		var ent = Utils.Lots.entry(jwkSetPublicJson, signedJWT.serialize());
		System.out.println(ent.getValue());
		System.out.println(sw.getTime());
		return ent;
	}

	private static void assertEquals(Object v1, Object v2) {
		Validate.isTrue(Objects.equals(v1, v2));

	}

}