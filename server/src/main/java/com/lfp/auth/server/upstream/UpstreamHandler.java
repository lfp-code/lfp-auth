package com.lfp.auth.server.upstream;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.time.StopWatch;

import com.google.gson.JsonElement;
import com.lfp.auth.auth0.management.config.Auth0ManagementConfig;
import com.lfp.auth.server.config.AuthServerConfig;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.handler.MessageHandler;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.interceptor.FollowRedirectsInterceptor;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.mikael.urlbuilder.UrlBuilder;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.http.HttpMethod;

public class UpstreamHandler implements HttpHandler {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final List<String> IGNORE_REQUEST_HEADER_NAMES = List.of(Headers.HOST, Headers.CONNECTION,
			Headers.CONTENT_LENGTH, Headers.ORIGIN, Headers.REFERER);

	private final MemoizedSupplier<OkHttpClient> clientSupplier = Utils.Functions.memoize(() -> {
		OkHttpClient.Builder cb;
		if (MachineConfig.isDeveloper())
			cb = Ok.Clients.get(Proxy.builder(URI.create("http://localhost:8888")).build()).newBuilder();
		else
			cb = Ok.Clients.get().newBuilder();
		cb.followRedirects(false);
		cb.addInterceptor(new FollowRedirectsInterceptor());
		cb.addInterceptor(chain -> {
			var request = chain.request();
			var requestHost = request.url().host();
			if (upstreamMatch(request))
				return chain.proceed(request);
			Entry<Bytes, HeaderMap> responseEntry = MessageHandler.getResponseEntry(getInvalidHostBody(requestHost),
					Ok.Calls.streamHeaders(request));
			var responseBody = responseEntry.getKey();
			var responseHeaders = responseEntry.getValue();
			var mediaType = Headers.tryGetMediaType(responseHeaders).orElse(null);
			var okResponseBody = Ok.Calls
					.createResponseBody(MediaType.parse(mediaType == null ? null : mediaType.toString()), responseBody);
			return Ok.Calls.createResponse(request, StatusCodes.FORBIDDEN, okResponseBody);
		});
		return cb.build();
	});
	private final URI upstreamURI;

	public UpstreamHandler() {
		URI uri = Configs.get(Auth0ManagementConfig.class).uri();
		if (uri == null)
			this.upstreamURI = null;
		else {
			var urlb = UrlBuilder.fromUri(uri).withPath(null).withFragment(null);
			this.upstreamURI = URIs.normalize(urlb.toUri());
		}
	}

	@Override
	public void handleRequest(HttpServerExchange hse) throws Exception {
		if (this.upstreamURI == null) {
			new MessageHandler(StatusCodes.FORBIDDEN, "upstream service is not available").handleRequest(hse);
			return;
		}
		var sw = MachineConfig.isDeveloper() ? StopWatch.createStarted() : null;
		var request = toRequest(hse);
		try (var response = this.clientSupplier.get().newCall(request).execute();) {
			writeResponse(response, hse);
		} finally {
			if (sw != null)
				logger.info("handle time:{}", sw.getTime());
		}
	}

	private boolean upstreamMatch(Request request) {
		if (this.upstreamURI == null)
			return false;
		var requestURL = request.url();
		if (!Utils.Strings.equalsIgnoreCase(this.upstreamURI.getHost(), requestURL.host()))
			return false;
		URI requestURINormalized;
		{
			var urlb = UrlBuilder.fromUri(requestURL.uri());
			urlb = urlb.withPath(null);
			urlb = urlb.withFragment(null);
			requestURINormalized = URIs.normalize(urlb.toUri());
		}
		return this.upstreamURI.equals(requestURINormalized);
	}

	private Request toRequest(HttpServerExchange hse) {
		var rb = new Request.Builder();
		rb = rb.url(buildUpstreamURI(hse).toString());
		{
			for (var ent : UndertowUtils.streamHeaders(hse.getRequestHeaders())) {
				if (Utils.Lots.stream(IGNORE_REQUEST_HEADER_NAMES).anyMatch(v -> v.equalsIgnoreCase(ent.getKey())))
					continue;
				rb = rb.header(ent.getKey(), ent.getValue());
			}
		}
		{
			var method = hse.getRequestMethod().toString().toUpperCase();
			RequestBody requestBody;
			if (!HttpMethod.permitsRequestBody(method))
				requestBody = null;
			else {
				var mediaType = Headers.tryGetMediaType(UndertowUtils.streamHeaders(hse.getRequestHeaders()))
						.map(v -> MediaType.parse(v.toString())).orElse(null);
				requestBody = Ok.Calls.createRequestBody(mediaType, () -> {
					hse.startBlocking();
					var is = hse.getInputStream();
					return modifyRequestBody(is);
				});
			}
			rb.method(method, requestBody);
		}
		return rb.build();
	}

	private URI buildUpstreamURI(HttpServerExchange hse) {
		var urlb = UrlBuilder.fromUri(UndertowUtils.getRequestURI(hse));
		urlb = urlb.withScheme(this.upstreamURI.getScheme());
		urlb = urlb.withHost(this.upstreamURI.getHost());
		var port = URIs.normalizePort(this.upstreamURI.getScheme(), this.upstreamURI.getPort());
		urlb = urlb.withPort(port == -1 ? null : port);
		var userInfo = Utils.Strings.trimToNull(upstreamURI.getUserInfo());
		if (userInfo != null)
			urlb = urlb.withUserInfo(userInfo);
		return urlb.toUri();
	}

	private InputStream modifyRequestBody(InputStream inputStream) throws IOException {
		if (inputStream == null)
			return Utils.Bits.emptyInputStream();
		var upstreamClientIdAppendPeekBytes = Configs.get(AuthServerConfig.class).upstreamClientIdAppendPeekBytes();
		return Utils.Bits.peek(inputStream, upstreamClientIdAppendPeekBytes, is -> {
			var bytes = Utils.Bits.from(is);
			var je = Utils.Functions.catching(() -> Serials.Gsons.fromBytes(bytes, JsonElement.class), t -> null);
			if (je == null)
				return bytes.inputStream();
			if (!modifyJson(je))
				return bytes.inputStream();
			return Serials.Gsons.toBytes(je).inputStream();
		});
	}

	private boolean modifyJson(JsonElement je) {
		if (!je.isJsonObject())
			return false;
		var auth0AdminConfig = Configs.get(Auth0ManagementConfig.class);
		var clientSecretKey = auth0AdminConfig.clientSecretKey();
		var clientSecret = Serials.Gsons.tryGetAsString(je, clientSecretKey).orElse(null);
		if (Utils.Strings.isBlank(clientSecret))
			return false;
		var clientIdKey = auth0AdminConfig.clientIdKey();
		var clientId = Serials.Gsons.tryGetAsString(je, clientIdKey).orElse(null);
		if (Utils.Strings.isNotBlank(clientId))
			return false;
		clientId = auth0AdminConfig.clientId();
		if (Utils.Strings.isBlank(clientId))
			return false;
		var jo = je.getAsJsonObject();
		jo.remove(clientIdKey);
		jo.addProperty(clientIdKey, clientId);
		return true;
	}

	private void writeResponse(Response response, HttpServerExchange hse) throws IOException {
		hse.setStatusCode(response.code());
		var responseHeaders = hse.getResponseHeaders();
		for (var ent : Ok.Calls.headerMap(response))
			responseHeaders = responseHeaders.add(HttpString.tryFromString(ent.getKey()), ent.getValue());
		hse.startBlocking();
		try (var body = response.body();
				var is = body == null ? null : body.byteStream();
				var os = hse.getOutputStream();) {
			if (is != null)
				Utils.Bits.copy(is, os);
		} finally {
			hse.endExchange();
		}
	}

	private static String getInvalidHostBody(String requestHost) {
		var msg = String.format("%s - invalid upstream host:%s", StatusCodes.FORBIDDEN_STRING, requestHost);
		return msg;
	}
}
