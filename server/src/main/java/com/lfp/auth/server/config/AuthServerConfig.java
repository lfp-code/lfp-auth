package com.lfp.auth.server.config;

import java.net.URI;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.auth.service.config.AuthServiceConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;

public interface AuthServerConfig extends Config {

	@DefaultValue("/v2/logout")
	String upstreamLogoutPath();

	@DefaultValue("returnTo")
	String upstreamLogoutReturnToParamName();

	@DefaultValue("512000") // 512kb
	long upstreamClientIdAppendPeekBytes();

	@DefaultValue(AuthServiceConfig.PATH_BASE + "/logout")
	String logoutPath();

	@DefaultValue(AuthServiceConfig.PATH_BASE + "/tfa")
	String traefikForwardAuthPath();

	List<String> authorizedIssuerHosts();

	@ConverterClass(URIConverter.class)
	List<URI> authorizedIssuerURIs();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.propertiesBuilder().withSkipPopulated(true).build());
		Configs.printProperties(PrintOptions.jsonBuilder().withSkipPopulated(true).build());
	}
}
