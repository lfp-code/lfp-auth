package com.lfp.auth.auth0.auth.extension;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.security.DigestOutputStream;
import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.immutables.value.Value;
import org.reactivestreams.Publisher;
import org.redisson.api.RRateLimiterReactive;
import org.redisson.api.RateIntervalUnit;
import org.redisson.api.RateType;
import org.redisson.api.RedissonClient;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalListener;
import com.github.mizosoft.methanol.Methanol;
import com.google.gson.JsonSyntaxException;
import com.lfp.auth.auth0.auth.extension.ImmutableAuthorizationExtensionGroupCache.AuthorizationExtensionGroupCacheOptions;
import com.lfp.auth.auth0.auth.extension.config.AuthExtensionServiceImplConfig;
import com.lfp.auth.service.types.Group;
import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.reactor.Monos;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.mikael.urlbuilder.UrlBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.publisher.Sinks.Many;
import reactor.core.scheduler.Schedulers;

@ValueLFP.Style
@Value.Enclosing
public class AuthorizationExtensionGroupCache extends Scrapable.Impl {

	public static AuthorizationExtensionGroupCache getInstance() {
		return Instances.get(AuthorizationExtensionGroupCache.class, () -> {
			var options = AuthorizationExtensionGroupCacheOptions.builder().addIdentifiers("default").build();
			return new AuthorizationExtensionGroupCache(options);
		});
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final boolean VERBOSE_LOGGING = MachineConfig.isDeveloper() && true;
	private final Many<Bytes> updateNotifierSink = Sinks.many().multicast().directAllOrNothing();
	private final AtomicBoolean initialized = new AtomicBoolean();
	private final AuthorizationExtensionGroupCacheOptions options;
	private final AsyncLoadingCache<Optional<Void>, List<Group>> memoryCache;

	protected AuthorizationExtensionGroupCache(AuthorizationExtensionGroupCacheOptions options) {
		super();
		this.options = options;
		RemovalListener<Optional<Void>, List<Group>> removalListener = (key, value, cause) -> {
			emitOnUpdate(value);
		};
		this.memoryCache = Caffeine.newBuilder().removalListener(removalListener)
				.refreshAfterWrite(this.options.refreshAfterWrite()).executor(this.options.executor())
				.buildAsync((nil, e) -> {
					if (VERBOSE_LOGGING)
						LOGGER.info("load started");
					var loadFuture = asyncLoad(SubmitterExecutorAdapter.adaptExecutor(e)).collectList().toFuture();
					loadFuture.whenComplete((v, t) -> {
						if (VERBOSE_LOGGING)
							LOGGER.info("load complete");
					});
					return loadFuture;
				});
		var refreshPollingDelay = Duration
				.ofMillis(Math.max(500, this.options.refreshAfterWrite().dividedBy(4).toMillis()));
		var refreshPollingFuture = FutureUtils.executeWhile(() -> {
			var mono = getAll().count();
			if (initialized.get())
				mono = mono.delayElement(refreshPollingDelay);
			return Threads.Futures.asListenable(mono.toFuture());
		}, nil -> !this.isScrapped());
		Threads.Futures.logFailureError(refreshPollingFuture, true, "refresh polling error");
		this.onScrap(() -> refreshPollingFuture.cancel(true));
	}

	public Mono<Group> get(String groupId) {
		if (groupId == null)
			return Mono.empty();
		return getAll().filter(group -> {
			return group.streamIds().anyMatch(groupId::equals);
		}).next();
	}

	public Flux<Group> getAll() {
		return Monos.fromFuture(() -> {
			this.validateNotScrapped();
			return this.memoryCache.get(Optional.empty());
		}).flatMapIterable(Function.identity());
	}

	public Flux<Bytes> getUpdateNotifier() {
		return this.updateNotifierSink.asFlux();
	}

	private Flux<Group> asyncLoad(SubmitterExecutor executor) {
		var validate = !initialized.compareAndSet(false, true);
		var loadFlux = asyncLoadFile(executor, validate);
		loadFlux = loadFlux.switchIfEmpty(deferLocked(() -> {
			return asyncLoadURI(executor).thenMany(asyncLoadFile(executor, false));
		}));
		return loadFlux.publishOn(Schedulers.fromExecutor(executor));
	}

	private Flux<Group> asyncLoadFile(SubmitterExecutor executor, boolean validate) {
		Callable<List<Group>> loadFileTask = () -> {
			var file = this.options.diskCacheFile();
			if (!file.exists())
				return null;
			if (validate && file.lastModifiedElapsed(this.options.refreshPollingInterval()))
				return null;
			try (var is = this.options.diskCacheFile().inputStream();) {
				var response = Serials.Gsons.fromStream(is, GroupsResponse.class);
				return List.copyOf(response.groups);
			} catch (JsonSyntaxException e) {
				var error = e.getMessage();
				if (MachineConfig.isDeveloper())
					try (var reader = file.inputStream().reader()) {
						error += " - " + reader.readAll();
					}
				LOGGER.warn("disk cache read failed. error:{}", error);
				file.delete();
				return null;
			}
		};
		var loadFlux = Monos.fromFuture(() -> executor.submit(loadFileTask)).flatMapIterable(Function.identity());
		return loadFlux;
	}

	private Mono<Void> asyncLoadURI(SubmitterExecutor executor) {
		var requestMono = this.options.rateLimiter().acquire().then(Monos.fromFuture(executor.submit(() -> {
			var authorizationHeader = Headers.bearerAuthorization(this.options.authorizationHeaderSupplier().get());
			return HttpRequests.request().uri(this.options.groupsURI()).header(Headers.AUTHORIZATION,
					authorizationHeader);
		})));
		var responseMono = requestMono.map(request -> {
			if (VERBOSE_LOGGING)
				LOGGER.info("uri request:{}", request);
			return this.options.httpClient().sendAsync(request, BodyHandlers.ofInputStream());
		}).flatMap(Monos::fromFuture);
		Mono<Void> loadFlux = responseMono.flatMap(response -> Mono.fromCallable(() -> {
			writeToFile(response);
			return null;
		}));
		return loadFlux;
	}

	private <U> Flux<U> deferLocked(Supplier<? extends Publisher<U>> supplier) {
		Objects.requireNonNull(supplier);
		var lockName = this.options.generateKey("lock");
		var lock = this.options.redissonClient().reactive().getLock(lockName);
		var lockMono = lock.lock().flatMap(nil -> Mono.<U>empty());
		return lockMono.concatWith(Flux.defer(() -> {
			if (VERBOSE_LOGGING)
				LOGGER.info("lock acquired");
			// we have the lock if here without error;
			var unlockMono = lock.forceUnlock().filter(Boolean.FALSE::equals).flatMap(nil -> {
				return Mono.<U>error(new IllegalMonitorStateException("unlock failed"));
			});
			Flux<U> lockedFlux = Flux.defer(supplier);
			Function<Throwable, Mono<U>> fallback = t0 -> {
				return unlockMono.onErrorMap(t1 -> Throws.combine(t1, t0)).then(Mono.error(t0));
			};
			lockedFlux = lockedFlux.onErrorResume(fallback);
			lockedFlux = lockedFlux.concatWith(unlockMono);
			return lockedFlux;
		}));
	}

	private void writeToFile(HttpResponse<InputStream> response) throws IOException {
		var tempFile = Utils.Files.tempFile(THIS_CLASS, VERSION, Utils.Crypto.getRandomString());
		try {
			var md = Utils.Crypto.getMessageDigestMD5();
			try (var is = response.body();
					var os = tempFile.outputStream();
					var dos = new DigestOutputStream(os, md);) {
				StatusCodes.validate(response.statusCode(), 2);
				is.transferTo(dos);
			}
			var tempFileHash = Bytes.from(md.digest());
			var file = this.options.diskCacheFile();
			if (file.exists()) {
				var fileHash = Utils.Crypto.hashMD5(file);
				if (Objects.equals(tempFileHash, fileHash))
					return;
			}
			file.delete();
			FileUtils.moveFile(tempFile, file);
			LOGGER.info("group data updated");
		} finally {
			tempFile.delete();
			if (VERBOSE_LOGGING)
				LOGGER.info("uri response:{}", response);
		}
	}

	private void emitOnUpdate(List<Group> previousGroups) {
		Function<List<Group>, Bytes> hasher = v -> {
			if (v == null)
				return null;
			var md = Utils.Crypto.getMessageDigestMD5();
			try (var os = OutputStream.nullOutputStream(); var dos = new DigestOutputStream(os, md)) {
				Serials.Gsons.toStream(v, dos);
			} catch (IOException e) {
				throw Throws.unchecked(e);
			}
			return Bytes.from(md.digest());
		};
		var groups = this.memoryCache.get(Optional.empty()).join();
		var groupsHash = hasher.apply(groups);
		if (groupsHash == null)
			return;
		var previousGroupsHash = hasher.apply(previousGroups);
		if (Objects.equals(previousGroupsHash, groupsHash))
			return;
		this.updateNotifierSink.tryEmitNext(groupsHash);
	}

	@Value.Immutable
	public static abstract class AbstractAuthorizationExtensionGroupCacheOptions {

		public abstract List<Object> identifiers();

		@Value.Default
		public ThrowingSupplier<String, IOException> authorizationHeaderSupplier() {
			return () -> {
				var authorizationHeaderValue = AuthExtensionTokenSupplier.INSTANCE.getAuthorizationHeaderValue();
				return authorizationHeaderValue;
			};
		}

		@Value.Default
		public URI groupsURI() {
			var urlb = UrlBuilder.fromUri(AuthExtensionServiceImplConfig.apiURI());
			urlb = urlb.withPath(urlb.path + "/groups");
			return urlb.toUri();
		}

		@Value.Default
		public Executor executor() {
			return CoreTasks.executor();
		}

		@Value.Default
		public Duration refreshAfterWrite() {
			return Duration.ofSeconds(5);
		}

		@Value.Default
		public long rateLimiterRate() {
			return 1;
		}

		@Value.Default
		public Duration rateLimiterInterval() {
			return Duration.ofSeconds(3);
		}

		@Nullable
		public abstract Duration refreshPollingInterval();

		@Value.Default
		public RedissonClient redissonClient() {
			return RedissonClients.getDefault();
		}

		@Value.Lazy
		public Methanol httpClient() {
			return HttpClients.builder().executor(executor()).build();
		}

		@Value.Lazy
		public RRateLimiterReactive rateLimiter() {
			var name = generateKey("rate-limiter", rateLimiterRate(), rateLimiterInterval().toMillis());
			var rateLimiter = redissonClient().reactive().getRateLimiter(name);
			rateLimiter.trySetRate(RateType.OVERALL, rateLimiterRate(), rateLimiterInterval().toSeconds(),
					RateIntervalUnit.SECONDS);
			return rateLimiter;
		}

		@Value.Lazy
		public FileExt diskCacheFile() {
			var name = generateKey("disk-cache");
			return Utils.Files.tempFile(THIS_CLASS, VERSION, name);
		}

		@Value.Check
		AbstractAuthorizationExtensionGroupCacheOptions check() {
			if (this.refreshPollingInterval() == null) {
				var refreshPollingIntervalMillis = Math.max(500, this.refreshAfterWrite().dividedBy(4).toMillis());
				return AuthorizationExtensionGroupCacheOptions.copyOf(this)
						.withRefreshPollingInterval(Duration.ofMillis(refreshPollingIntervalMillis));
			}
			return this;
		}

		public String generateKey(Object... prepend) {
			var keyParts = Streams.<Object>of(THIS_CLASS, VERSION).append(prepend).append(identifiers());
			return RedisUtils.generateKey(keyParts.toArray());
		}
	}

	private static class GroupsResponse {

		public List<Group> groups;

		public Long total;

	}

	public static void main(String[] args) throws InterruptedException {
		var export = AuthorizationExtensionGroupCache.getInstance();
		System.err.println(export.getAll().count().block());
		new Thread(() -> {
			export.updateNotifierSink.asFlux().subscribe(v -> {
				System.out.println("update:" + v.encodeHex());
				System.err.println(export.getAll().count().block());
			});
		}).start();
		for (int i = 0;; i++) {
			if (i > 0)
				Thread.sleep(1_000);
			var sw = StopWatch.createStarted();
			System.out.println(export.getAll().count().block() + " - " + sw.getTime());
		}
	}
}
