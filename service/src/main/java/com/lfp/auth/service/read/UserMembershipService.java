package com.lfp.auth.service.read;

import java.util.Map.Entry;

import com.lfp.auth.service.types.UserMemberships;

public interface UserMembershipService extends AuthReadService<Entry<String, Boolean>, UserMemberships> {

}
