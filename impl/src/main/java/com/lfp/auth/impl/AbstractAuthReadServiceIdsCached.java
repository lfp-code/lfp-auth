package com.lfp.auth.impl;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.Executor;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.lfp.auth.impl.cache.AbstractAuthCache;
import com.lfp.auth.impl.cache.AuthCacheOptions;
import com.lfp.auth.service.read.AuthReadService;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingRunnable;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public abstract class AbstractAuthReadServiceIdsCached<K, V> extends AbstractAuthReadServiceCached<K, V>
		implements AuthReadService.Ids<K, V> {

	private final AbstractAuthCache<Nada, List<K>> idCache;

	protected AbstractAuthReadServiceIdsCached(int batchSize, boolean concurrent, TypeToken<K> keyTypeToken,
			AuthCacheOptions<V> valueCacheOptions) {
		super(batchSize, concurrent, keyTypeToken, valueCacheOptions);
		this.idCache = buildIdCache(keyTypeToken, valueCacheOptions);
	}

	protected AbstractAuthReadServiceIdsCached(int batchSize, Supplier<Executor> requestExecutorSupplier,
			TypeToken<K> keyTypeToken, AuthCacheOptions<V> valueCacheOptions) {
		super(batchSize, requestExecutorSupplier, keyTypeToken, valueCacheOptions);
		this.idCache = buildIdCache(keyTypeToken, valueCacheOptions);
	}

	@Override
	public StreamEx<K> getIds() {
		var ids = this.getIdCache().get(Nada.get());
		return AuthImplUtils.normalizeStream(ids);
	}

	@SuppressWarnings("serial")
	protected AbstractAuthCache<Nada, List<K>> buildIdCache(TypeToken<K> keyTypeToken,
			AuthCacheOptions<V> valueCacheOptions) {
		TypeToken<List<K>> keyListTypeToken;
		{
			keyListTypeToken = new TypeToken<List<K>>() {
			}.where(new TypeParameter<K>() {
			}, keyTypeToken);
		}
		var optionsb = valueCacheOptions.withValueTypeToken(keyListTypeToken).toBuilder();
		optionsb.storageKeyParts(
				Utils.Lots.stream(valueCacheOptions.getStorageKeyParts()).append("id", "cache").toList());
		var idCache = new AbstractAuthCache<Nada, List<K>>(optionsb.build()) {

			@Override
			protected List<K> loadFreshValue(@NonNull Nada key) throws Exception {
				StreamEx<K> stream = AuthImplUtils
						.normalizeStream(AbstractAuthReadServiceIdsCached.this.loadFreshIds());
				return stream.toList();
			}

		};
		return idCache;
	}

	@Override
	protected Iterable<? extends Entry<String, ThrowingRunnable<Exception>>> getRefreshPollTasks(
			Predicate<String> refreshPollingTaskIdExistsPredicate) {
		var taskId = "ids";
		if (refreshPollingTaskIdExistsPredicate.test(taskId))
			return List.of();
		ThrowingRunnable<Exception> task = () -> {
			StreamEx<K> idStream = AuthImplUtils.normalizeStream(this.loadFreshIds());
			this.getIdCache().put(Nada.get(), idStream.toList());
		};
		return EntryStream.of(taskId, task);
	}

	protected AbstractAuthCache<Nada, List<K>> getIdCache() {
		return this.idCache;
	}

	protected abstract Iterable<? extends K> loadFreshIds();

}
