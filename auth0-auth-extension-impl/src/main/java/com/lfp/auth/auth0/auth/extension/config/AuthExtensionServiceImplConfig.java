package com.lfp.auth.auth0.auth.extension.config;

import java.net.URI;
import java.time.Duration;

import org.aeonbits.owner.Config;
import org.apache.commons.lang3.Validate;

import com.lfp.auth.auth0.management.config.Auth0ManagementConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.properties.converter.DurationConverter;
import com.lfp.joe.utils.Utils;

public interface AuthExtensionServiceImplConfig extends Config {

	@DefaultValue("us")
	String region();

	@DefaultValue("/adf6e2f2b84784b57522e3b19dfc9201/api")
	String apiPath();

	@DefaultValue("25")
	int batchSize();

	// unsure, assuming 10 per second
	// https://auth0.com/docs/troubleshoot/customer-support/operational-policies/rate-limit-policy/management-api-endpoint-rate-limits
	@DefaultValue("50")
	long rateLimiterRate();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("5 second")
	Duration rateLimiterInterval();

	default URI apiURI(String tenant) {
		tenant = Utils.Strings.trimToNullOptional(tenant).orElseThrow(IllegalArgumentException::new);
		var region = Utils.Strings.trimToNullOptional(region()).orElseThrow(IllegalArgumentException::new);
		var apiPath = URIs.normalizePath(apiPath());
		Validate.notBlank(apiPath);
		return URI.create(String.format("https://%s.%s.webtask.io%s", tenant, region, apiPath));
	}

	public static URI apiURI() {
		var tenant = Configs.get(Auth0ManagementConfig.class).tenant();
		return Configs.get(AuthExtensionServiceImplConfig.class).apiURI(tenant);
	}

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.propertiesBuilder().withSkipPopulated(true).build());
		Configs.printProperties(PrintOptions.jsonBuilder().withSkipPopulated(true).build());
	}
}
