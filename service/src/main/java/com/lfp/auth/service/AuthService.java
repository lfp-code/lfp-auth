package com.lfp.auth.service;

import java.util.List;

import com.lfp.auth.service.config.AuthServiceConfig;
import com.lfp.auth.service.types.Group;
import com.lfp.auth.service.types.User;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AuthService {

	public static final Class<AuthServiceConfig> CONFIG_TYPE = AuthServiceConfig.class;

	public static final String PATH_BASE = AuthServiceConfig.PATH_BASE;

	public static final String USER_PATH = PATH_BASE + "/user";
	public static final String USER_LOOKUP_PATH = USER_PATH + "/lookup";
	public static final String USER_LOOKUP_ALL_PATH = USER_LOOKUP_PATH + "/all";

	public static final String GROUP_PATH = PATH_BASE + "/group";
	public static final String GROUP_LOOKUP_PATH = GROUP_PATH + "/lookup";
	public static final String GROUP_LOOKUP_ALL_PATH = GROUP_LOOKUP_PATH + "/all";

	public static final String USER_GROUP_IDS_PATH = PATH_BASE + "/user-group-ids";
	public static final String USER_GROUP_IDS_LOOKUP_PATH = USER_GROUP_IDS_PATH + "/lookup";
	public static final String USER_GROUP_IDS_LOOKUP_ALL_PATH = USER_GROUP_IDS_LOOKUP_PATH + "/all";

	@GET(USER_PATH)
	StreamEx<User> getUsersSelf();

	@GET(USER_LOOKUP_ALL_PATH)
	StreamEx<User> getUsers();

	@POST(USER_LOOKUP_PATH)
	EntryStream<String, User> getUsers(@Field(value = "input") Iterable<? extends String> input);

	@GET(GROUP_PATH)
	StreamEx<Group> getGroupsSelf();

	@GET(GROUP_LOOKUP_ALL_PATH)
	StreamEx<Group> getGroups();

	@POST(GROUP_LOOKUP_PATH)
	EntryStream<String, Group> getGroups(@Field(value = "input") Iterable<? extends String> input);

	@GET(USER_GROUP_IDS_LOOKUP_ALL_PATH)
	EntryStream<String, List<String>> getUserGroupIds(@Field(value = "calculate") Boolean calculate);

	@POST(USER_GROUP_IDS_LOOKUP_PATH)
	EntryStream<String, List<String>> getUserGroupIds(@Field(value = "calculate") Boolean calculate,
			@Field(value = "input") Iterable<? extends String> input);

}
