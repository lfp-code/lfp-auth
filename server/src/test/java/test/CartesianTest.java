package test;

import java.util.Set;

import com.google.common.collect.Collections2;

public class CartesianTest {

	public static void main(String[] args) {
		var products = Collections2.permutations(Set.of("1", "2", "3"));
		for (var list : products)
			System.out.println(list);
	}
}
