package test;

import java.net.URI;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.Undertows;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;

import io.mikael.urlbuilder.UrlBuilder;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.CookieImpl;
import io.undertow.server.handlers.PathHandler;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import io.undertow.util.Methods;

public class ServerTest {

	private static URI loginURI(HttpServerExchange hse) {
		var requestURI = Optional.ofNullable(hse).map(v -> UndertowUtils.getRequestURI(v)).orElse(null);
		var redirectURIBuilder = UrlBuilder.fromString("https://auth-api.syncflux.co/callback");
		if (requestURI != null)
			redirectURIBuilder = redirectURIBuilder.addParameter("returnTo", requestURI.toString());
		var authorizeURI = UrlBuilder.fromString("https://auth.syncflux.co/authorize")
				.addParameter("client_id", "0enuv5y7elrTarKEWLPuaPlZVPkXBysZ")
				.addParameter("response_type", "code")
				.addParameter("redirect_uri", redirectURIBuilder.toUri().toString())
				.toUri();
		return authorizeURI;
	}

	public static void main(String[] args) throws InterruptedException {
		var pathHandler = new PathHandler();
		pathHandler.addPrefixPath("/", hse -> {
			var sw = StopWatch.createStarted();
			var uri = UndertowUtils.getRequestURI(hse);
			var lines = new ArrayList<Object>();
			lines.add(TLDs.parseDomainName(uri));
			lines.add(uri);
			UndertowUtils.streamHeaders(hse.getRequestHeaders())
					.sortedBy(ent -> ent.getKey().toLowerCase())
					.forEach(ent -> lines.add(String.format("%s - %s", ent.getKey(), ent.getValue())));
			var out = Streams.of(lines).joining("\n");
			out += String.format("\ntime:%s\n", sw.getTime());
			hse.getResponseSender().send(out);
		});
		pathHandler.addPrefixPath("/login", hse -> {
			var loginURI = loginURI(null);
			hse.setStatusCode(StatusCodes.FOUND);
			hse.getResponseHeaders().put(Headers.LOCATION, loginURI.toString());
			hse.getResponseSender().close();
		});
		pathHandler.addExactPath("/forward", hse -> {
			var uri = UndertowUtils.getRequestURI(hse);
			var uriRaw = UndertowUtils.getRequestURI(hse, true);
			for (var name : Streams.of(hse.getRequestHeaders().getHeaderNames())
					.sortedBy(v -> v.toString().toLowerCase()))
				for (var value : hse.getRequestHeaders().get(name))
					System.out.println(String.format("%s - %s", name, value));
			System.out.println(uri);
			System.out.println(uriRaw);
			System.out.println(hse.getHostAndPort());
			var authorization = hse.getRequestHeaders().getFirst(Headers.AUTHORIZATION);
			var jwt = Jwts.tryParseInsecure(authorization, true).orElse(null);
			if (jwt != null) {
				hse.getResponseHeaders()
						.put(HttpString.tryFromString("X-Forward-Auth-Claims"),
								Serials.Gsons.get().toJson(jwt.getBody(), Map.class));
				return;
			}
			System.out.println(hse.getRequestURI());
			System.out.println(hse.getRequestURL());
			System.out.println(hse.getQueryString());
			hse.setResponseCookie(new CookieImpl("neato", "speedo-" + System.currentTimeMillis())
					.setDomain("." + TLDs.parseDomainName(uri))
					.setHttpOnly(true)
					.setSecure(URIs.isSecure(uri)));

			var authorizeURI = UrlBuilder.fromString("https://auth.syncflux.co/authorize")
					.addParameter("client_id", "67ybgdE4VpXhHZQckSsdowkop3YEN2sj")
					.addParameter("response_type", "code")
					.addParameter("redirect_uri",
							UrlBuilder.fromString("https://auth-api.syncflux.co/callback")
									.addParameter("returnTo", uri.toString())
									.toString())
					.toUri();
			System.out.println(authorizeURI);
			hse.setStatusCode(StatusCodes.FOUND);
			hse.getResponseHeaders().put(Headers.LOCATION, authorizeURI.toString());
			hse.getResponseSender().close();
		});
		HttpHandler postHandler = hse -> {
			if (Methods.POST.equals(hse.getRequestMethod()))
				System.out.println("POST");
			pathHandler.handleRequest(hse);
		};
		var server = Undertows.serverBuilder(8888).setHandler(postHandler).build();
		server.start();
		System.out.println(server.getListenerInfo());
		Thread.currentThread().join();
	}

}
