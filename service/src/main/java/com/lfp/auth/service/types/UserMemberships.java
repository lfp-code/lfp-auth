package com.lfp.auth.service.types;

import java.util.List;
import java.util.Map;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutablePreBuild;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

@BeanDefinition(hierarchy = "immutable")
public class UserMemberships extends AbstractMemberRecord {

	@PropertyDefinition(get = "", set = "")
	private transient final Object _nil;

	public String getUserId() {
		return super.getBeanId();
	}

	public boolean containsGroupId(String id) {
		return super.containsId(id);
	}

	public List<String> getGroupIds() {
		return super.getValues();
	}

	public StreamEx<String> streamGroupIds() {
		return super.streamValues();
	}

	@ImmutablePreBuild
	private static void preBuild(Builder builder) {
		preBuildProtected(builder);
	}

	protected static void preBuildProtected(Builder builder) {
		AbstractMemberRecord.preBuildProtected(builder);
	}

	private static StreamEx<String> streamUserIdClaimKeys() {
		var stream = Streams.of(User.meta().sub(), User.meta().userId()).flatMap(JodaBeans::streamNames);
		stream = stream.append(Streams.of(() -> Streams.of(BeanRef.$(UserMemberships::getUserId).getName())));
		stream = stream.mapPartial(Utils.Strings::trimToNullOptional).distinct();
		return stream;
	}

	private static StreamEx<String> streamGroupIdsClaimKeys() {
		var stream = Streams.of(() -> Streams.of(BeanRef.$(UserMemberships::getGroupIds).getName()));
		stream = stream.mapPartial(Utils.Strings::trimToNullOptional).distinct();
		return stream;
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code UserMemberships}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static UserMemberships.Meta meta() {
		return UserMemberships.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(UserMemberships.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static UserMemberships.Builder builder() {
		return new UserMemberships.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected UserMemberships(UserMemberships.Builder builder) {
		super(builder);
		this._nil = builder._nil;
	}

	@Override
	public UserMemberships.Meta metaBean() {
		return UserMemberships.Meta.INSTANCE;
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	@Override
	public Builder toBuilder() {
		return new Builder(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			UserMemberships other = (UserMemberships) obj;
			return JodaBeanUtils.equal(_nil, other._nil) && super.equals(obj);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = hash * 31 + JodaBeanUtils.hashCode(_nil);
		return hash ^ super.hashCode();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(64);
		buf.append("UserMemberships{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	@Override
	protected void toString(StringBuilder buf) {
		super.toString(buf);
		buf.append("_nil").append('=').append(JodaBeanUtils.toString(_nil)).append(',').append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code UserMemberships}.
	 */
	public static class Meta extends AbstractMemberRecord.Meta {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code _nil} property.
		 */
		private final MetaProperty<Object> _nil = DirectMetaProperty.ofImmutable(this, "_nil", UserMemberships.class,
				Object.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this,
				(DirectMetaPropertyMap) super.metaPropertyMap(), "_nil");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 2939218: // _nil
				return _nil;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public UserMemberships.Builder builder() {
			return new UserMemberships.Builder();
		}

		@Override
		public Class<? extends UserMemberships> beanType() {
			return UserMemberships.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code _nil} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Object> _nil() {
			return _nil;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 2939218: // _nil
				return ((UserMemberships) bean)._nil;
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code UserMemberships}.
	 */
	public static class Builder extends AbstractMemberRecord.Builder {

		private Object _nil;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(UserMemberships beanToCopy) {
			super(beanToCopy);
			this._nil = beanToCopy._nil;
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 2939218: // _nil
				return _nil;
			default:
				return super.get(propertyName);
			}
		}

		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 2939218: // _nil
				this._nil = newValue;
				break;
			default:
				super.set(propertyName, newValue);
				break;
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public UserMemberships build() {
			preBuild(this);
			return new UserMemberships(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the _nil.
		 * 
		 * @param _nil the new value
		 * @return this, for chaining, not null
		 */
		public Builder _nil(Object _nil) {
			this._nil = _nil;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(64);
			buf.append("UserMemberships.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		@Override
		protected void toString(StringBuilder buf) {
			super.toString(buf);
			buf.append("_nil").append('=').append(JodaBeanUtils.toString(_nil)).append(',').append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
