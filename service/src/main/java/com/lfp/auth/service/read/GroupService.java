package com.lfp.auth.service.read;

import com.lfp.auth.service.types.Group;

public interface GroupService extends AuthReadService.Ids<String, Group> {

}
