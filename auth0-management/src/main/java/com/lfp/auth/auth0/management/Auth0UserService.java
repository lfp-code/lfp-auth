package com.lfp.auth.auth0.management;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.auth0.exception.APIException;
import com.auth0.exception.Auth0Exception;
import com.lfp.auth.impl.AbstractAuthReadService;
import com.lfp.auth.service.read.UserService;
import com.lfp.auth.service.types.HasIds;
import com.lfp.auth.service.types.User;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;

import one.util.streamex.StreamEx;

public class Auth0UserService extends AbstractAuthReadService<String, User> implements UserService {

	private final ManagementAPILFP managementAPI;

	public Auth0UserService(ManagementAPILFP managementAPI) {
		super(1, true);
		this.managementAPI = Objects.requireNonNull(managementAPI);
	}

	protected ManagementAPILFP getManagementAPI() {
		return managementAPI;
	}

	@Override
	public StreamEx<String> getIds() {
		return HasIds.streamEntries(this.get()).keys();
	}

	@Override
	protected Iterable<? extends User> getInternal() {
		return this.managementAPI.streamUsers().map(Auth0Utils::asUser);
	}

	@Override
	protected Map<String, User> getInternal(List<String> keyBatch) throws Auth0Exception {
		Map<String, User> result = new HashMap<>();
		var keyIter = Streams.of(keyBatch).filter(v -> Jwts.tryParseInsecure(v).isEmpty()).iterator();
		while (keyIter.hasNext()) {
			var key = keyIter.next();
			com.auth0.json.mgmt.users.User auth0User;
			try {
				auth0User = this.managementAPI.users().get(key, null).execute();
			} catch (APIException e) {
				if (e.getStatusCode() == 404)
					continue;
				throw e;
			}
			var user = Auth0Utils.asUser(auth0User);
			if (user != null)
				result.put(key, user);
		}
		return result;
	}

	public static void main(String[] args) throws Auth0Exception {
		var user = new Auth0UserService(ManagementAPILFP.INSTANCE).getManagementAPI().users()
				.get("auth0|5991d51ed43c5e5ff0d71ecd", null).execute();
		System.out.println(Serials.Gsons.getPretty().toJson(user));
		System.out.println(Serials.Gsons.getPretty().toJson(Auth0Utils.asUser(user)));
	}

}
