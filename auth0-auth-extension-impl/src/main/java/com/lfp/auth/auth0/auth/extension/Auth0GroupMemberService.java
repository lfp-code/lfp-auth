package com.lfp.auth.auth0.auth.extension;

import java.time.Duration;
import java.util.HashSet;
import java.util.Optional;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.auth.impl.DelegatingGroupMemberService;
import com.lfp.auth.service.read.UserMembershipService;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

public enum Auth0GroupMemberService implements DelegatingGroupMemberService {
	INSTANCE;

	@Override
	public UserMembershipService getDelegate() {
		return Auth0UserMembershipService.INSTANCE;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws InterruptedException {
		var service = Auth0GroupMemberService.INSTANCE;
		var lookupGroupId = service.get().mapPartial(v -> {
			var userIds = v.streamUserIds().toList();
			return userIds.size() >= 5 ? Optional.of(v.getGroupId()) : Optional.empty();
		}).findFirst().get();
		for (int i = 0; i < 10; i++) {
			if (i > 0)
				Threads.sleep(Duration.ofSeconds(1));
			var count = 0;
			var groupTracker = new HashSet<>();
			var totalSw = StopWatch.createStarted();
			var umStream = service.get(Utils.Lots.entry(lookupGroupId, true)).values();
			for (var um : umStream) {
				for (var groupId : um.streamUserIds()) {
					count++;
					groupTracker.add(groupId);
					var msg = String.format("%s - %s - %s - %s - %s", count, groupTracker.size(), groupId,
							um.isCalculate(), um.getGroupId());
					System.out.println(msg);
				}
			}
			System.out.println(String.format("pass complete:%s %s", i, totalSw.getTime()));
		}
		System.exit(0);
	}

}
