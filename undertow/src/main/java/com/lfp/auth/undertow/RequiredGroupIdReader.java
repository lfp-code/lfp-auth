package com.lfp.auth.undertow;

import java.util.List;
import java.util.function.Function;

import com.lfp.auth.undertow.config.AuthUndertowConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

import io.undertow.server.HttpServerExchange;
import one.util.streamex.StreamEx;

public class RequiredGroupIdReader implements Function<HttpServerExchange, StreamEx<String>> {

	public static final RequiredGroupIdReader INSTANCE;
	static {
		var cfg = Configs.get(AuthUndertowConfig.class);
		INSTANCE = new RequiredGroupIdReader(cfg.requiredGroupIdsQueryParamName());
	}

	private final List<String> queryParamNames;

	public RequiredGroupIdReader(Iterable<String> queryParamNames) {
		this.queryParamNames = Utils.Lots.stream(queryParamNames).mapPartial(Utils.Strings::trimToNullOptional)
				.toImmutableList();
	}

	@Override
	public StreamEx<String> apply(HttpServerExchange hse) {
		if (this.queryParamNames.isEmpty())
			return StreamEx.empty();
		var qp = hse.getQueryParameters();
		if (qp == null || qp.isEmpty())
			return StreamEx.empty();
		var queryParamValueQueues = Utils.Lots.stream(this.queryParamNames)
				.map(v -> Utils.Lots.stream(qp).filterKeys(v::equalsIgnoreCase).values());
		var queryParamValues = Utils.Lots.flatMapAll(queryParamValueQueues).mapPartial(Utils.Strings::toStringOptional);
		queryParamValues = queryParamValues.mapPartial(Utils.Strings::trimToNullOptional);
		return queryParamValues;
	}

}
