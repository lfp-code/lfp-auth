package com.lfp.auth.undertow.config;

import java.util.List;

import org.aeonbits.owner.Config;

public interface AuthUndertowConfig extends Config {

	@DefaultValue("requiredGroupId")
	List<String> requiredGroupIdsQueryParamName();

	@DefaultValue("Authorization")
	List<String> tokenHeaderNames();

	@DefaultValue("access_token")
	List<String> tokenCookieNames();

	List<String> tokenQueryParamNames();
}
