package test;

import java.time.Duration;
import java.util.List;

import org.immutables.value.Value;
import org.redisson.api.RedissonClient;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

@ValueLFP.Style
@Value.Immutable
public abstract class AuthorizationExtensionCacheOptionsDef {

	@Value.Default
	public RedissonClient client() {
		return RedissonClients.getDefault();
	}

	public abstract List<Object> identifiers();

	@Value.Default
	public Duration cacheTimeToLive() {
		return Duration.ofDays(7);
	}

	public String generateKey(Object... prepend) {
		var keyParts = Streams.<Object>of(AuthorizationExtensionCacheOptions.class).append(prepend)
				.append(cacheTimeToLive().toMillis()).append(identifiers());
		return RedisUtils.generateKey(keyParts.toArray());
	}

	@Value.Check
	void validate() {
		Streams.of(identifiers()).nonNull().filter(v -> {
			if (v instanceof CharSequence && Utils.Strings.isBlank((CharSequence) v))
				return false;
			return true;
		}).findFirst().orElseThrow(() -> {
			return new IllegalArgumentException("identifier required");
		});
	}
}
