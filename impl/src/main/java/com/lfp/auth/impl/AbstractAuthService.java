package com.lfp.auth.impl;

import java.util.List;
import java.util.Map.Entry;

import com.lfp.auth.service.AuthService;
import com.lfp.auth.service.read.GroupService;
import com.lfp.auth.service.read.UserMembershipService;
import com.lfp.auth.service.read.UserService;
import com.lfp.auth.service.types.Group;
import com.lfp.auth.service.types.User;
import com.lfp.joe.stream.support.StreamExSupplier;
import com.lfp.joe.stream.Streams;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public abstract class AbstractAuthService implements AuthService {

	private static final Boolean USER_GROUP_IDS_CALCULATE_DEFAULT = true;
	private final UserService userService;
	private final GroupService groupService;
	private final UserMembershipService userMembershipService;

	public AbstractAuthService(UserService userService, GroupService groupService,
			UserMembershipService userMembershipService) {
		super();
		this.userService = userService;
		this.groupService = groupService;
		this.userMembershipService = userMembershipService;
	}

	@Override
	public StreamEx<User> getUsers() {
		if (this.userService == null || !isGetUsersAuthorized())
			return StreamEx.empty();
		return this.userService.get();
	}

	protected abstract boolean isGetUsersAuthorized();

	@Override
	public EntryStream<String, User> getUsers(Iterable<? extends String> input) {
		if (this.userService == null)
			return EntryStream.empty();
		StreamExSupplier<String> streamSupplier = AuthImplUtils.normalizeStream(input).chain(Streams.cached());
		if (!isGetUsersAuthorized(streamSupplier.get()))
			return EntryStream.empty();
		var estream = this.userService.get(streamSupplier.get());
		return estream;
	}

	protected abstract boolean isGetUsersAuthorized(StreamEx<String> input);

	@Override
	public StreamEx<Group> getGroups() {
		if (this.groupService == null || !isGetGroupsAuthorized())
			return StreamEx.empty();
		return this.groupService.get();
	}

	protected abstract boolean isGetGroupsAuthorized();

	@Override
	public EntryStream<String, Group> getGroups(Iterable<? extends String> input) {
		if (this.groupService == null)
			return EntryStream.empty();
		StreamExSupplier<String> streamSupplier = AuthImplUtils.normalizeStream(input).chain(Streams.cached());
		if (!isGetGroupsAuthorized(streamSupplier.get()))
			return EntryStream.empty();
		return this.groupService.get(streamSupplier.get());
	}

	protected abstract boolean isGetGroupsAuthorized(StreamEx<String> input);

	@Override
	public EntryStream<String, List<String>> getUserGroupIds(Boolean calculate) {
		if (calculate == null)
			return getUserGroupIds(USER_GROUP_IDS_CALCULATE_DEFAULT);
		if (this.userMembershipService == null || !isGetUserGroupIdsAuthorized(calculate))
			return EntryStream.empty();
		var uidStream = this.getUsers().flatMap(User::streamIds).distinct();
		return userMembershipService.get(uidStream.mapToEntry(v -> calculate)).mapKeys(Entry::getKey)
				.mapValues(v -> v.streamGroupIds().toList());
	}

	protected abstract boolean isGetUserGroupIdsAuthorized(boolean calculate);

	@Override
	public EntryStream<String, List<String>> getUserGroupIds(Boolean calculate, Iterable<? extends String> input) {
		if (calculate == null)
			return getUserGroupIds(USER_GROUP_IDS_CALCULATE_DEFAULT, input);
		if (this.userMembershipService == null)
			return EntryStream.empty();
		StreamExSupplier<String> streamSupplier = AuthImplUtils.normalizeStream(input).chain(Streams.cached());
		if (!isGetUserGroupIdsAuthorized(calculate, streamSupplier.get()))
			return EntryStream.empty();
		return userMembershipService.get(streamSupplier.get().mapToEntry(v -> calculate)).mapKeys(Entry::getKey)
				.mapValues(v -> v.streamGroupIds().toList());
	}

	protected abstract boolean isGetUserGroupIdsAuthorized(boolean calculate, StreamEx<String> input);

}
