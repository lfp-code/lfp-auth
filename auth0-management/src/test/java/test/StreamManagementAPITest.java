package test;

import com.lfp.auth.auth0.management.ManagementAPILFP;

public class StreamManagementAPITest {

	public static void main(String[] args) {
		ManagementAPILFP.INSTANCE.streamUsers().forEach(v -> {
			System.out.println("user: " + v.getEmail());
		});
		System.err.println("done");
		System.exit(0);
	}

}
