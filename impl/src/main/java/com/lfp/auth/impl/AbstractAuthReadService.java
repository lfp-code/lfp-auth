package com.lfp.auth.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.function.Supplier;

import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.auth.service.read.AuthReadService;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.CompletionNotifier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public abstract class AbstractAuthReadService<K, V> implements AuthReadService<K, V> {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final CompletionNotifier<Nada> onFirstUseNotifier = CompletionNotifier.create();
	private int batchSize;
	private Supplier<SubmitterExecutor> requestExecutorSupplier;

	protected AbstractAuthReadService(int batchSize, boolean concurrent) {
		this(batchSize, !concurrent ? null : () -> AuthImplUtils.createRefreshPollingExecutor());
	}

	protected AbstractAuthReadService(int batchSize, Supplier<Executor> requestExecutorSupplier) {
		this.batchSize = batchSize;
		this.requestExecutorSupplier = () -> {
			return Optional.ofNullable(requestExecutorSupplier).map(Supplier::get)
					.map(SubmitterExecutorAdapter::adaptExecutor).orElseGet(SameThreadSubmitterExecutor::instance);
		};
	}

	@Override
	public StreamEx<V> get() {
		onFirstUseNotifier.complete(Nada.get());
		var ible = Utils.Functions.unchecked(() -> getInternal());
		return AuthImplUtils.normalizeStream(ible);
	}

	protected abstract Iterable<? extends V> getInternal() throws Exception;

	@Override
	public EntryStream<K, V> get(Iterable<? extends K> keyIble) {
		onFirstUseNotifier.complete(Nada.get());
		StreamEx<K> keyStream = AuthImplUtils.normalizeStream(keyIble);
		var keyBatchStream = keyStream.chain(Streams.buffer(this.batchSize));
		var executor = requestExecutorSupplier.get();
		var futureStream = keyBatchStream.map(keyBatch -> {
			return SubmitterExecutorAdapter.adaptExecutor(executor).submit(() -> {
				if (MachineConfig.isDeveloper()) {
					// logger.info("keyBatch started:{}", keyBatch);
				}
				return getInternal(keyBatch);
			});
		});
		var keyBatchResultStream = Threads.Futures.completeStream(futureStream, false, true);
		keyBatchResultStream = keyBatchResultStream.nonNull();
		keyBatchResultStream = keyBatchResultStream.map(keyBatch -> {
			if (MachineConfig.isDeveloper()) {
				// logger.info("keyBatch done:{}", keyBatch);
			}
			return keyBatch;
		});
		var keyValueStream = keyBatchResultStream.chain(Streams.flatMap(v -> v.entrySet().stream()));
		keyValueStream = AuthImplUtils.normalizeStream(keyValueStream);
		return keyValueStream.chain(EntryStreams::of);
	}

	protected CompletionNotifier<Nada> getOnFirstUseNotifier() {
		return onFirstUseNotifier;
	}

	protected abstract Map<K, V> getInternal(List<K> keyBatch) throws Exception;

}