package com.lfp.auth.database;

import java.util.Objects;

import org.apache.commons.lang3.Validate;

public class MetadataService {

	private final AuthDatastore authDatastore;
	private final String idKey;
	private final String mapKey;

	public MetadataService(AuthDatastore authDatastore, String idKey, String mapKey) {
		super();
		this.authDatastore = Objects.requireNonNull(authDatastore);
		this.idKey = Validate.notBlank(idKey);
		this.mapKey = Validate.notBlank(mapKey);
	}

}
