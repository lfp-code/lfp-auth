package com.lfp.auth.auth0.traefik.forward.auth.core;

import java.util.Map;

import javax.annotation.Nullable;
import javax.validation.constraints.NotEmpty;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import com.github.throwable.beanref.BeanPath;
import com.lfp.joe.core.classpath.MemberCache.Invoker;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.utils.Utils;

@Gson.TypeAdapters
@Value.Immutable
public interface ForwardAuthApplication {

	@NotEmpty
	String getName();

	@Nullable
	String getClientId();

	@Nullable
	String getClientSecret();

	@Nullable
	String getAudience();

	@Nullable
	String getScope();

	@Nullable
	String getRedirectUri();

	@Nullable
	String getTokenCookieDomain();

	@Nullable
	String getReturnTo();

	@Nullable
	String[] getRestrictedMethods();

	@Nullable
	String[] getRequiredPermissions();

	@Nullable
	String[] getClaims();

	@Value.Check
	default ForwardAuthApplication validate() {
		//Validate.notBlank(getName(), "name required - %s", this);
		var result = ImmutableForwardAuthApplication.copyOf(this);
		if (Utils.Strings.isBlank(result.getScope()))
			return result.withScope("profile openid email");
		for (var ent : Const.STRING_ARRAY_INVOKERS.entrySet()) {
			var bp = ent.getKey();
			var invoker = ent.getValue();
			var value = bp.get(result);
			if (value == null)
				return invoker.invoke(result, Const.EMPTY_STRING_ARRAY_ARGUMENTS);
		}
		for (var ent : Const.EMPTY_IF_BLANK_INVOKERS.entrySet()) {
			var bp = ent.getKey();
			var invoker = ent.getValue();
			var value = bp.get(result);
			if (Utils.Strings.isBlank(value))
				return invoker.invoke(result, "");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	static enum Const {
		;

		private static final Object[] EMPTY_STRING_ARRAY_ARGUMENTS = new Object[] { EmptyArrays.of(String.class) };

		private static final Map<BeanPath<ImmutableForwardAuthApplication, String[]>, Invoker<ImmutableForwardAuthApplication, ImmutableForwardAuthApplication>> STRING_ARRAY_INVOKERS;
		static {
			var beanPaths = ForwardAuthConfigs
					.stringArrayBeanPaths(ImmutableForwardAuthApplication.class);
			STRING_ARRAY_INVOKERS = beanPaths.mapToEntry(v -> ForwardAuthConfigs.invoker(v))
					.toImmutableMap();
		}

		private static final Map<BeanPath<ImmutableForwardAuthApplication, String>, Invoker<ImmutableForwardAuthApplication, ImmutableForwardAuthApplication>> EMPTY_IF_BLANK_INVOKERS;
		static {
			var beanPaths = ForwardAuthConfigs
					.charSequenceBeanPaths(ImmutableForwardAuthApplication.class)
					.filter(v -> String.class.isAssignableFrom(v.getType()))
					.map(v -> (BeanPath<ImmutableForwardAuthApplication, String>) v);
			EMPTY_IF_BLANK_INVOKERS = beanPaths.mapToEntry(v -> ForwardAuthConfigs.invoker(v))
					.toImmutableMap();
		}
	}
}