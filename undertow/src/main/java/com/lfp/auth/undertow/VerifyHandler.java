package com.lfp.auth.undertow;

import java.util.Objects;
import java.util.Optional;
import java.util.function.BiPredicate;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.connect.undertow.handler.MessageHandler;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.utils.Utils;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class VerifyHandler implements HttpHandler {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final BiPredicate<Iterable<? extends String>, Iterable<? extends String>> verifyService;
	private final HttpHandler successHandler;
	private final HttpHandler needsGroupMembershipHandler;
	private final HttpHandler needsSignInHandler;

	public VerifyHandler(BiPredicate<Iterable<? extends String>, Iterable<? extends String>> verifyService,
			HttpHandler successHandler, HttpHandler needsGroupMembershipHandler, HttpHandler needsSignInHandler) {
		super();
		this.verifyService = Objects.requireNonNull(verifyService);
		this.successHandler = Optional.ofNullable(successHandler).orElseGet(() -> new MessageHandler(StatusCodes.OK));
		this.needsGroupMembershipHandler = Optional.ofNullable(needsGroupMembershipHandler)
				.orElseGet(() -> new MessageHandler(StatusCodes.UNAUTHORIZED));
		this.needsSignInHandler = Optional.ofNullable(needsSignInHandler)
				.orElseGet(() -> new MessageHandler(StatusCodes.UNAUTHORIZED));
	}

	@Override
	public void handleRequest(HttpServerExchange hse) throws Exception {
		var sw = MachineConfig.isDeveloper() ? StopWatch.createStarted() : null;
		var jwtIble = Utils.Lots.asCollection(JwtCandidateReader.INSTANCE.apply(hse));
		var requiredGroupIds = Utils.Lots.stream(RequiredGroupIdReader.INSTANCE.apply(hse))
				.mapPartial(Utils.Strings::trimToNullOptional).nonNull().toList();
		var verified = this.verifyService.test(jwtIble, requiredGroupIds);
		HttpHandler handler;
		if (verified)
			handler = this.successHandler;
		else if (this.verifyService.test(jwtIble, null))
			handler = this.needsGroupMembershipHandler;
		else
			handler = this.needsSignInHandler;
		handleRequest(handler, hse);
		if (sw != null)
			logger.info("handle time:{}", sw.getTime());
	}

	protected void handleRequest(HttpHandler handler, HttpServerExchange hse) throws Exception {
		handler.handleRequest(hse);
		hse.endExchange();
	}

}
