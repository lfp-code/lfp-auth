package com.lfp.auth.auth0.auth.extension;

import java.io.IOException;

import com.lfp.auth.auth0.management.AuthAdminTokenSupplier;
import com.lfp.joe.net.http.oauth.OauthRequest;
import com.lfp.joe.net.http.oauth.OauthRequestImpl;

public class AuthExtensionTokenSupplier extends AuthAdminTokenSupplier {

	public static final AuthExtensionTokenSupplier INSTANCE = new AuthExtensionTokenSupplier();

	@Override
	protected OauthRequest buildOauthRequest() {
		var requestBuilder = OauthRequestImpl.build(super.buildOauthRequest()).toBuilder();
		requestBuilder.audience("urn:auth0-authz-api");
		return requestBuilder.build();
	}

	public static void main(String[] args) throws IOException {
		var value = AuthExtensionTokenSupplier.INSTANCE.getAuthorizationHeaderValue();
		System.out.println(value);
	}
}
