package com.lfp.auth.service.types;

import java.util.LinkedHashMap;
import java.util.Map;

import com.lfp.joe.serial.Serials;

public class Metadata extends LinkedHashMap<String, Object> {

	public Map<String, Object> clone() {
		var je = Serials.Gsons.get().toJsonTree(this);
		return Serials.Gsons.get().fromJson(je, Metadata.class);
	}

}
