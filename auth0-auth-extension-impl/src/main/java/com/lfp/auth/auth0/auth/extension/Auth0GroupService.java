package com.lfp.auth.auth0.auth.extension;

import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.auth.database.GroupMetadataService;
import com.lfp.auth.impl.AbstractAuthReadService;
import com.lfp.auth.service.read.GroupService;
import com.lfp.auth.service.types.Group;
import com.lfp.auth.service.types.HasIds;
import com.lfp.auth.service.types.Metadata;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;

import one.util.streamex.StreamEx;

public class Auth0GroupService extends AbstractAuthReadService<String, Group> implements GroupService {

	public static final Auth0GroupService INSTANCE = new Auth0GroupService();

	protected Auth0GroupService() {
		super(1, true);
	}

	@Override
	public StreamEx<String> getIds() {
		return HasIds.streamEntries(this.get()).keys();
	}

	@Override
	protected Iterable<? extends Group> getInternal() throws Exception {
		var groupFlux = AuthorizationExtensionGroupCache.getInstance().getAll();
		var groupStream = groupFlux.as(Fluxi::toStream);
		groupStream = groupStream.nonNull();
		groupStream = appendMetadata(groupStream);
		return groupStream;
	}

	@Override
	protected Map<String, Group> getInternal(List<String> keyBatch) throws Exception {
		Map<String, Group> result = new LinkedHashMap<>();
		for (var group : getInternal()) {
			for (var groupId : group.streamIds()) {
				if (!keyBatch.contains(groupId))
					continue;
				result.put(groupId, group);
			}
		}
		result = appendMetadata(result);
		return result;
	}

	protected Map<String, Group> appendMetadata(Map<String, Group> result) {
		if (!(result instanceof LinkedHashMap))
			return appendMetadata(new LinkedHashMap<>(result));
		appendMetadata(result.values()).forEach(mdGroup -> {
			mdGroup.streamIds().forEach(gid -> {
				result.computeIfPresent(gid, (nilk, nilv) -> mdGroup);
			});
		});
		return result;
	}

	protected StreamEx<Group> appendMetadata(Iterable<? extends Group> groups) {
		var groupListStream = Streams.of(groups).nonNull().chain(Streams.buffer());
		return groupListStream.map(v -> appendMetadata(v)).flatMap(Function.identity());
	}

	protected StreamEx<Group> appendMetadata(List<? extends Group> groups) {
		var gidStream = HasIds.streamEntries(groups).keys().distinct();
		var gidToGroupMetadata = GroupMetadataService.INSTANCE.get(gidStream).distinctKeys().toMap();
		return Streams.of(groups).map(group -> {
			var md = group.getGroupMetadata();
			if (md == null)
				md = new Metadata();
			for (var gid : group.streamIds()) {
				var groupMetadataRecord = gidToGroupMetadata.get(gid);
				if (groupMetadataRecord == null)
					continue;
				var values = groupMetadataRecord.getValues();
				if (values == null)
					continue;
				var estream = Streams.of(values.entrySet()).chain(EntryStreams::of).nonNullKeys().nonNullValues();
				for (var ent : estream)
					md.put(ent.getKey(), ent.getValue());
			}
			return group.toBuilder().groupMetadata(md).build();
		});
	}

	public static void main(String[] args) throws InterruptedException {
		var service = new Auth0GroupService();
		for (int i = 0; i < 50; i++) {
			if (i > 0)
				Threads.sleep(Duration.ofSeconds(1));
			var sw = StopWatch.createStarted();
			var count = service.get("30005a79-7bac-4d71-b7ee-ecb756a7c216", "0e7115eb-67b4-4c5c-8a38-21f644e7495e")
					.count();
			var time = sw.getTime();
			System.out.println(String.format("%s - %s", count, time));
		}
		System.exit(0);
	}

}
