package com.lfp.auth.auth0.auth.extension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import com.lfp.auth.auth0.management.Auth0UserServiceCached;
import com.lfp.auth.impl.AbstractAuthReadService;
import com.lfp.auth.service.read.UserMembershipService;
import com.lfp.auth.service.read.UserService;
import com.lfp.auth.service.types.HasIds;
import com.lfp.auth.service.types.UserMemberships;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class Auth0UserMembershipService extends AbstractAuthReadService<Entry<String, Boolean>, UserMemberships>
		implements UserMembershipService {

	public static final Auth0UserMembershipService INSTANCE = new Auth0UserMembershipService(
			Auth0UserServiceCached.INSTANCE);

	private final UserService userService;

	protected Auth0UserMembershipService(UserService userService) {
		super(1, true);
		this.userService = Objects.requireNonNull(userService);
	}

	@Override
	protected Iterable<? extends UserMemberships> getInternal() throws Exception {
		var idStream = this.userService.getIds();
		var keyStreams = idStream.map(v -> {
			return EntryStream.of(v, true, v, false);
		});
		var keyStream = Utils.Lots.flatMap(keyStreams).chain(Utils.Lots::streamEntries);
		return this.get(keyStream).values();
	}

	@Override
	protected Map<Entry<String, Boolean>, UserMemberships> getInternal(List<Entry<String, Boolean>> keyBatch)
			throws Exception {
		Map<Entry<String, Boolean>, UserMemberships> keyToUM = new HashMap<>();
		for (var key : keyBatch) {
			var groupIds = getGroupIds(key.getKey(), key.getValue());
			var umBuilder = UserMemberships.builder();
			umBuilder.beanId(key.getKey());
			umBuilder.calculate(key.getValue());
			umBuilder.values(groupIds.toList());
			keyToUM.put(key, umBuilder.build());
		}
		return keyToUM;
	}

	protected StreamEx<String> getGroupIds(String userId, boolean calculate) {
		var groupStream = AuthorizationExtensionMemberCache.getInstance().getGroups(userId, calculate)
				.as(Fluxi::toStream);
		var groupIdStream = groupStream.flatMap(group -> {
			return group.streamIds();
		});
		return HasIds.streamIds(groupIdStream);
	}

}
