package com.lfp.auth.server;

import java.util.Objects;

import com.lfp.auth.auth0.auth.extension.Auth0GroupService;
import com.lfp.auth.auth0.auth.extension.Auth0UserMembershipService;
import com.lfp.auth.auth0.management.Auth0UserServiceCached;
import com.lfp.auth.impl.AbstractAuthService;
import com.lfp.auth.impl.AuthImplUtils;
import com.lfp.auth.server.config.AuthServerConfig;
import com.lfp.auth.service.AuthService;
import com.lfp.auth.service.types.Group;
import com.lfp.auth.service.types.HasIds;
import com.lfp.auth.service.types.User;
import com.lfp.auth.undertow.JwtCandidateReader;
import com.lfp.connect.undertow.retrofit.RetrofitHandler;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.resolver.IssuerJwkSigningKeyResolver;
import com.lfp.joe.jwt.resolver.SigningKeyRequestFilters;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import io.jsonwebtoken.SigningKeyResolver;
import one.util.streamex.StreamEx;

public class AuthServiceImpl extends AbstractAuthService {

	private final SigningKeyResolver signingKeyResolver;
	private final MemoizedSupplier<RetrofitHandler<AuthService>> retrofitHandlerSupplier;

	public AuthServiceImpl() {
		this(IssuerJwkSigningKeyResolver.of(
				SigningKeyRequestFilters.issuerURIMatches(Configs.get(AuthServerConfig.class).authorizedIssuerURIs())));
	}

	public AuthServiceImpl(SigningKeyResolver signingKeyResolver) {
		super(Auth0UserServiceCached.INSTANCE, Auth0GroupService.INSTANCE, Auth0UserMembershipService.INSTANCE);
		this.signingKeyResolver = Objects.requireNonNull(signingKeyResolver);
		this.retrofitHandlerSupplier = Utils.Functions
				.memoize(() -> new RetrofitHandler<AuthService>(AuthService.class, this));
	}

	@Override
	public StreamEx<User> getUsersSelf() {
		var hse = this.getRetrofitHandler().getCurrentHttpServerExchange().orElse(null);
		var jwtCandidates = JwtCandidateReader.INSTANCE.apply(hse);
		var userIdStream = jwtCandidates.flatMap(v -> parseUserIds(v));
		var userStream = getUsers(userIdStream).values();
		return userStream.chain(HasIds.distinctNonNull());
	}

	@Override
	public StreamEx<Group> getGroupsSelf() {
		var currentHttpServerExchange = this.getRetrofitHandler().getCurrentHttpServerExchange();
		System.out.println(currentHttpServerExchange.isPresent());
		return StreamEx.empty();
	}

	@Override
	protected boolean isGetUsersAuthorized() {
		return true;
	}

	@Override
	protected boolean isGetUsersAuthorized(StreamEx<String> input) {
		return true;
	}

	@Override
	protected boolean isGetGroupsAuthorized() {
		return true;
	}

	@Override
	protected boolean isGetGroupsAuthorized(StreamEx<String> input) {
		return true;
	}

	@Override
	protected boolean isGetUserGroupIdsAuthorized(boolean calculate) {
		return true;
	}

	@Override
	protected boolean isGetUserGroupIdsAuthorized(boolean calculate, StreamEx<String> input) {
		return true;
	}

	public RetrofitHandler<AuthService> getRetrofitHandler() {
		return retrofitHandlerSupplier.get();
	}

	protected StreamEx<String> parseUserIds(String jwtInput) {
		if (Utils.Strings.isBlank(jwtInput))
			return Streams.empty();
		if (this.signingKeyResolver == null)
			return Streams.empty();
		var jws = Jwts.tryParse(jwtInput, this.signingKeyResolver).orElse(null);
		if (jws == null)
			return Streams.empty();
		var claims = jws.getBody();
		if (claims == null)
			return Streams.empty();
		var claimNames = JodaBeans.streamNames(User.meta().sub()).append(JodaBeans.streamNames(User.meta().userId()));
		claimNames = AuthImplUtils.normalizeStream(claimNames);
		var claimValues = claimNames.map(v -> claims.get(v, String.class));
		claimValues = AuthImplUtils.normalizeStream(claimValues);
		return claimValues;
	}

}
