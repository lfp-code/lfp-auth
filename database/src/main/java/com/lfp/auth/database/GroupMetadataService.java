package com.lfp.auth.database;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

import com.lfp.auth.service.read.AuthReadService;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@SuppressWarnings("deprecation")
public class GroupMetadataService implements AuthReadService.Ids<String, GroupMetadataRecord> {

	public static final GroupMetadataService INSTANCE = new GroupMetadataService(AuthDatastore.get());

	private final AuthDatastore authDatastore;

	protected GroupMetadataService(AuthDatastore authDatastore) {
		super();
		this.authDatastore = Objects.requireNonNull(authDatastore);
	}

	@Override
	public StreamEx<String> getIds() {
		var query = this.authDatastore.find(GroupMetadataRecord.class).retrievedFields(true,
				GroupMetadataRecord.meta().groupId().name());
		return Streams.of(query).map(v -> v.getGroupId());
	}

	@Override
	public StreamEx<GroupMetadataRecord> get() {
		return Streams.of(authDatastore.find(GroupMetadataRecord.class));
	}

	@Override
	public EntryStream<String, GroupMetadataRecord> get(Iterable<? extends String> groupIds) {
		var gidStream = Streams.of(groupIds).filter(Utils.Strings::isNotBlank).distinct();
		var gidListStream = gidStream.chain(Streams.buffer());
		return gidListStream.map(v -> {
			var query = this.authDatastore.find(GroupMetadataRecord.class)
					.field(GroupMetadataRecord.meta().groupId().name()).in(v);
			return Streams.of(query);
		}).chain(Streams.flatMap()).mapToEntry(v -> v.getGroupId()).invert();
	}

	public StreamEx<GroupMetadataRecord> getRecords(String metadataKey, Object value) {
		return getRecords(metadataKey, Arrays.asList(value));
	}

	public StreamEx<GroupMetadataRecord> getRecords(String metadataKey, Collection<?> values) {
		if (Utils.Strings.isBlank(metadataKey) || values == null || values.isEmpty())
			return Streams.empty();
		var query = this.authDatastore.find(GroupMetadataRecord.class)
				.field(GroupMetadataRecord.meta().values().name() + "." + metadataKey).in(values);
		return Streams.of(query);
	}

	public StreamEx<GroupMetadataRecord> getRecordsContaining(String metadataKey, String value, boolean ignoreCase) {
		if (Utils.Strings.isBlank(metadataKey))
			return Streams.empty();
		var queryField = this.authDatastore.find(GroupMetadataRecord.class)
				.field(GroupMetadataRecord.meta().values().name() + "." + metadataKey);
		var query = ignoreCase ? queryField.containsIgnoreCase(value) : queryField.contains(value);
		return Streams.of(query);
	}

	public static void main(String[] args) {
		GroupMetadataService.INSTANCE.getIds().forEach(v -> {
			System.out.println(Serials.Gsons.getPretty().toJson(v));
		});
	}

}
