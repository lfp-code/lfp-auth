package com.lfp.auth.auth0.management;

import java.util.Objects;

import org.apache.commons.lang3.time.StopWatch;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.google.common.reflect.TypeToken;
import com.lfp.auth.impl.AbstractAuthReadServiceIdsCached;
import com.lfp.auth.impl.cache.AuthCacheOptions;
import com.lfp.auth.service.read.UserService;
import com.lfp.auth.service.types.HasIds;
import com.lfp.auth.service.types.User;

public class Auth0UserServiceCached extends AbstractAuthReadServiceIdsCached<String, User> implements UserService {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 5;
	public static final Auth0UserServiceCached INSTANCE;
	static {
		var optionsb = AuthCacheOptions.builder(User.class, THIS_CLASS, VERSION, "default");
		var service = new Auth0UserServiceCached(new Auth0UserService(ManagementAPILFP.INSTANCE), optionsb.build());
		INSTANCE = service;
	}

	private final UserService delegate;

	protected Auth0UserServiceCached(UserService delegate, AuthCacheOptions<User> options) {
		super(1, true, TypeToken.of(String.class), options);
		this.delegate = Objects.requireNonNull(delegate);
		super.startRefreshPolling();
	}

	@Override
	protected Iterable<? extends User> getInternal() throws Exception {
		return this.get(getIds()).values();
	}

	@Override
	protected @Nullable User loadFreshValue(@NonNull String key) throws Exception {
		return this.delegate.get(key).values().findFirst().orElse(null);
	}

	@Override
	protected Iterable<? extends String> loadFreshIds() {
		var idStream = HasIds.streamEntries(this.delegate.get()).map(ent -> {
			var userId = ent.getKey();
			var user = ent.getValue();
			this.getValueCache().put(userId, user);
			return userId;
		}).distinct();
		return idStream;
	}

	public static void main(String[] args) throws InterruptedException {
		var service = Auth0UserServiceCached.INSTANCE;
		var list = service.delegate.get().toList();
		System.out.println(list.size());
		for (int i = 0; i < 50; i++) {
			if (i > 0) {
				// Threads.sleep(Duration.ofSeconds(1));
				Thread.currentThread().join();
			}

			var sw = StopWatch.createStarted();
			var count = service.get().count();
			var time = sw.getTime();
			System.out.println(String.format("%s - %s", count, time));
		}
		System.exit(0);
	}

}
