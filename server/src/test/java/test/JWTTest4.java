package test;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import com.lfp.joe.jwk.Jwks;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.serial.Serials;

public class JWTTest4 {

	public static void main(String[] args)
			throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException,
			InvalidKeyException {
		var json = Serials.Gsons.getPretty().toJson(Jwks.toJwk(Jwks.createRSAKey()));
		System.out.println(json);
		var jwk = Serials.Gsons.get().fromJson(json, JwkLFP.class);
		System.out.println(jwk);
		System.out.println(jwk.getPublicKey());
	}

}
