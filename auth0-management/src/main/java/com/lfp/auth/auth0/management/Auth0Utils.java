package com.lfp.auth.auth0.management;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.auth.service.types.User;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public class Auth0Utils {

	private static final MemoizedSupplier<ObjectMapper> OBJECT_MAPPER_S = Utils.Functions.memoize(() -> {
		var objectMapper = new ObjectMapper();
		return objectMapper;
	});
	private static final MemoizedSupplier<Gson> SERIALIZER_GSON_S = Utils.Functions.memoize(() -> {
		var gsonBuilder = Serials.Gsons.getBuilder();
		gsonBuilder.registerTypeHierarchyAdapter(Date.class, DateSerializer.INSTANCE);
		gsonBuilder.registerTypeHierarchyAdapter(Collection.class, CollectionSerializer.INSTANCE);
		gsonBuilder.registerTypeHierarchyAdapter(Map.class, MapSerializer.INSTANCE);
		return gsonBuilder.create();
	});

	public static ObjectMapper getObjectMapper() {
		return OBJECT_MAPPER_S.get();
	}

	public static User asUser(com.auth0.json.mgmt.users.User auth0User) {
		if (auth0User == null)
			return null;
		var barr = Throws.unchecked(() -> getObjectMapper().writeValueAsBytes(auth0User));
		var user = Serials.Gsons.fromBytes(Utils.Bits.from(barr), User.class);
		return user;
	}

	public static com.auth0.json.mgmt.users.User asAuth0User(User user) {
		if (user == null)
			return null;
		var bytes = Serials.Gsons.toBytes(SERIALIZER_GSON_S.get(), user);
		return Utils.Functions.unchecked(() -> {
			return getObjectMapper().readValue(bytes.array(), com.auth0.json.mgmt.users.User.class);
		});
	}

	private static enum DateSerializer implements JsonSerializer<Date> {
		INSTANCE;

		@Override
		public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
			if (src == null)
				return JsonNull.INSTANCE;
			return Serials.Gsons.getStandard().toJsonTree(src);
		}

	}

	private static enum CollectionSerializer implements JsonSerializer<Collection<?>> {
		INSTANCE;

		@Override
		public JsonElement serialize(Collection<?> src, Type typeOfSrc, JsonSerializationContext context) {
			if (src == null || src.isEmpty())
				return JsonNull.INSTANCE;
			return context.serialize(src);
		}

	}

	private static enum MapSerializer implements JsonSerializer<Map<?, ?>> {
		INSTANCE;

		@Override
		public JsonElement serialize(Map<?, ?> src, Type typeOfSrc, JsonSerializationContext context) {
			if (src == null || src.isEmpty())
				return JsonNull.INSTANCE;
			return context.serialize(src);
		}

	}

}
