package test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.auth.server.AuthServiceImpl;
import com.lfp.rsocket.client.RSocketClients;

public class AuthExtensionServiceTest {

	public static void main(String[] args) {
		var client = RSocketClients.get(AuthServiceImpl.class);
		for (int i = 0; i < 50; i++) {
			var sw = StopWatch.createStarted();
			var list = new ArrayList<String>(client.userIds().collect(Collectors.toList()));
			System.out.println(String.format("%s - %s", list.size(), sw.getTime()));
			Collections.shuffle(list);
			var userId = list.get(0);
			sw.reset();
			var groupIds = client.userTogroupMemberUserIds(true, userId).collect(Collectors.toList());
			System.out.println(String.format("%s - %s - %s", userId, groupIds.size(), sw.getTime()));
		}
	}

}
