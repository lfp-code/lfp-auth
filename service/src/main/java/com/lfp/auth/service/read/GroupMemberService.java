package com.lfp.auth.service.read;

import java.util.Map.Entry;

import com.lfp.auth.service.types.GroupMembers;

public interface GroupMemberService extends AuthReadService<Entry<String, Boolean>, GroupMembers> {

}
