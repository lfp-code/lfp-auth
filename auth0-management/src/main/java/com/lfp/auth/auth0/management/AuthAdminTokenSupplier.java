package com.lfp.auth.auth0.management;

import java.io.IOException;
import java.util.Optional;

import com.lfp.auth.auth0.management.config.Auth0ManagementConfig;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.oauth.OAuthTokenFunction;
import com.lfp.joe.net.http.oauth.OAuthTokenResponse;
import com.lfp.joe.net.http.oauth.OauthRequest;
import com.lfp.joe.net.http.oauth.OauthRequestImpl;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;

public class AuthAdminTokenSupplier extends OAuthTokenFunction.Cached {

	public static final AuthAdminTokenSupplier INSTANCE = new AuthAdminTokenSupplier();

	protected AuthAdminTokenSupplier() {
	}

	public OAuthTokenResponse get() throws IOException {
		OauthRequest request = buildOauthRequest();
		if (MachineConfig.isDeveloper())
			request = OauthRequestImpl.build(request);
		var response = super.apply(request);
		return response;
	}

	public String getAuthorizationHeaderValue() throws IOException {
		return Optional.ofNullable(get()).map(OAuthTokenResponse::getAccessToken).filter(Utils.Strings::isNotBlank)
				.map(v -> String.format("Bearer %s", v)).orElse(null);
	}

	protected OauthRequest buildOauthRequest() {
		var cfg = Configs.get(Auth0ManagementConfig.class);
		var requestBuilder = OauthRequestImpl.build(cfg).toBuilder();
		var path = URIs.normalizePath(cfg.apiV2Path());
		if (!Utils.Strings.endsWith(path, "/"))
			path += "/";
		String audience = UrlBuilder.fromUri(cfg.uri()).withPath(path).toString();
		requestBuilder.audience(audience);
		return requestBuilder.build();
	}

	public static void main(String[] args) throws IOException {
		for (int i = 0; i < 10; i++) {
			var token = AuthAdminTokenSupplier.INSTANCE.getAuthorizationHeaderValue();
			System.out.println(token);
		}
	}

}
