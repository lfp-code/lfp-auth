package com.lfp.auth.auth0.auth.extension;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.immutables.value.Value;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.auth.auth0.auth.extension.ImmutableAuthorizationExtensionMemberCache.AuthorizationExtensionMemberCacheOptions;
import com.lfp.auth.service.types.Group;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.ImmutableEntry;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.Monos;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;

import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.scheduler.Schedulers;

@ValueLFP
@Value.Enclosing
public class AuthorizationExtensionMemberCache extends Scrapable.Impl {

	public static AuthorizationExtensionMemberCache getInstance() {
		return Instances.get(AuthorizationExtensionMemberCache.class, () -> {
			var options = AuthorizationExtensionMemberCacheOptions.builder().build();
			return new AuthorizationExtensionMemberCache(options);
		});
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final AuthorizationExtensionMemberCacheOptions options;
	private final @NonNull AsyncLoadingCache<String, Set<String>> memoryCache;

	protected AuthorizationExtensionMemberCache(AuthorizationExtensionMemberCacheOptions options) {
		super();
		this.options = Objects.requireNonNull(options);
		this.memoryCache = Caffeine.newBuilder().executor(this.options.executor())
				.refreshAfterWrite(this.options.refreshAfterWrite()).buildAsync((k, e) -> {
					return asyncLoad(k, e).collect(Collectors.toSet()).toFuture();
				});
		this.options.groupCache().getUpdateNotifier().subscribeOn(Scheduli.unlimited()).subscribe(nil -> {
			this.memoryCache.asMap().clear();
		});
	}

	public Flux<String> getUserIds(String groupId, boolean calculate) {
		if (groupId == null)
			return Flux.empty();
		if (!calculate)
			return this.options.groupCache().get(groupId).map(v -> Streams.of(v.getMembers()))
					.flatMapMany(Flux::fromStream);
		return Monos.fromFuture(() -> this.memoryCache.get(groupId)).flatMapIterable(Function.identity());
	}

	public Flux<Group> getGroups(String userId, boolean calculate) {
		if (userId == null)
			return Flux.empty();
		if (!calculate)
			return this.options.groupCache().getAll().filter(group -> {
				return Streams.of(group.getMembers()).anyMatch(userId::equals);
			});
		return getAll().flatMap(ent -> {
			var group = ent.getKey();
			var userIds = ent.getValue();
			return userIds.filter(userId::equals).next().map(nil -> group);
		});
	}

	public Flux<Entry<Group, Flux<String>>> getAll() {
		return this.options.groupCache().getAll().map(group -> {
			var userIdFlux = group.streamIds().map(groupId -> {
				return Monos.fromFuture(() -> this.memoryCache.get(groupId)).flatMapIterable(Function.identity());
			}).chain(Flux::fromStream).flatMap(Function.identity());
			userIdFlux = userIdFlux.distinct();
			return ImmutableEntry.of(group, userIdFlux);
		});
	}

	private Flux<String> asyncLoad(String groupId, Executor executor) {
		var flux = Flux.<String>create(sink -> {
			try {
				load(groupId, new HashSet<>(), sink);
				sink.complete();
			} catch (Throwable t) {
				sink.error(t);
			}
		});
		flux = flux.distinct();
		var scheduler = Schedulers.fromExecutor(executor);
		return flux.subscribeOn(scheduler);
	}

	private void load(String groupId, Set<String> visitedGroups, FluxSink<String> sink) {
		if (groupId == null || !visitedGroups.add(groupId))
			return;
		var group = this.options.groupCache().get(groupId).block();
		if (group == null)
			return;
		Streams.of(group.getMembers()).nonNull().forEach(sink::next);
		for (var subGroupId : Streams.of(group.getNested()))
			load(subGroupId, visitedGroups, sink);
	}

	@Value.Immutable
	public static abstract class AbstractAuthorizationExtensionMemberCacheOptions {

		@Value.Default
		public AuthorizationExtensionGroupCache groupCache() {
			return AuthorizationExtensionGroupCache.getInstance();
		}

		@Value.Default
		public Executor executor() {
			return CoreTasks.executor();
		}

		@Value.Default
		public Duration refreshAfterWrite() {
			return Duration.ofSeconds(15);
		}

	}

	public static void main(String[] args) throws InterruptedException {
		var groups = AuthorizationExtensionMemberCache.getInstance()
				.getGroups("google-oauth2|116397002027375308464", false).collectList().block();
		System.out.println(Serials.Gsons.getPretty().toJson(groups));
		var groupIds = AuthorizationExtensionGroupCache.getInstance().getAll().as(v -> Fluxi.toStream(v))
				.flatMap(Group::streamIds);
		groupIds = Streams.of("30005a79-7bac-4d71-b7ee-ecb756a7c216");
		var groupIdList = groupIds.toList();
		for (int i = 0;; i++) {
			if (i > 0)
				Thread.sleep(1_000);
			for (var calculate : List.of(false, true)) {
				for (var groupId : groupIdList) {
					var userIds = AuthorizationExtensionMemberCache.getInstance().getUserIds(groupId, calculate)
							.collectList().block();
					LOGGER.info("groupId:{} calculate:{} size:{}", groupId, calculate, userIds.size());
				}
			}
		}
	}
}
