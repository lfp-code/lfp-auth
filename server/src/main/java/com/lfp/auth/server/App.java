package com.lfp.auth.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;

import com.lfp.auth.server.config.AuthServerConfig;
import com.lfp.auth.server.local.LogoutHandler;
import com.lfp.auth.server.upstream.UpstreamHandler;
import com.lfp.auth.service.AuthService;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.config.UndertowConfig;
import com.lfp.connect.undertow.handler.ErrorLoggingHandler;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.connect.undertow.retrofit.RetrofitHandler;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.rsocket.server.RSocketServers;
import com.lfp.rsocket.server.proxy.RSocketServerProxies;

import io.rsocket.ipc.reflection.server.RequestHandlingRSocketReflection;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;

public class App {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Executor DISPATCH_EXECUTOR = Threads.Pools.centralPool()
			.limit(Utils.Machine.logicalProcessorCount() * 2);

	public static void main(String[] args) throws InterruptedException, IOException {
		Threads.Pools.centralPool().submit(() -> Serials.Gsons.get());
		InetSocketAddress listenAddress = Configs.get(UndertowConfig.class).defaultAddress();
		if (MachineConfig.isDeveloper())
			listenAddress = new InetSocketAddress(listenAddress.getHostString(), 6969);
		var rsocketProxyServer = RSocketServerProxies.create(listenAddress, null, null, null);
		var authService = new AuthServiceImpl();
		Undertow server;
		{
			HttpHandler handler = createPathHandler(authService.getRetrofitHandler());
			handler = new ErrorLoggingHandler(handler);
			handler = new ThreadHttpHandler(handler, DISPATCH_EXECUTOR);
			Undertow.Builder serverBuilder = Undertows.serverBuilder(rsocketProxyServer.getInternalAddress());
			server = serverBuilder.setHandler(handler).build();
		}
		server.start();
		RequestHandlingRSocketReflection requestHandlingRSocket = rsocketProxyServer.getRequestHandlingRSocket();
		RSocketServers.attach(requestHandlingRSocket, AuthServiceImpl.class, authService);
		logger.info("server startup complete. listenAddress:{}", listenAddress);
	}

	private static HttpHandler createPathHandler(RetrofitHandler<AuthService> retrofitHandler) {
		var cfg = Configs.get(AuthServerConfig.class);
		{
			var upstreamHandler = new UpstreamHandler();
			UndertowUtils.appendPath(retrofitHandler, "/", upstreamHandler);
		}
		{
			// var handler = new VerifyHandler(new VerifyServiceImpl(), null, null, null);
			// UndertowUtils.appendPath(pathHandler, cfg.traefikForwardAuthPath(), handler);
		}
		LogoutHandler.INSTANCE.accept(retrofitHandler);
		return retrofitHandler;
	}

}
