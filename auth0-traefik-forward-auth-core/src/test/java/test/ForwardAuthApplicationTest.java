package test;

import com.lfp.auth.auth0.traefik.forward.auth.core.ImmutableForwardAuthApplication;
import com.lfp.joe.serial.Serials;

public class ForwardAuthApplicationTest {

	public static void main(String[] args) {
		var result = ImmutableForwardAuthApplication.builder().name("").build();
		System.out.println(Serials.Gsons.getPretty().toJson(result));
	}
}
