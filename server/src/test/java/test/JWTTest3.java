package test;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class JWTTest3 {

	public static void main(String[] args)
			throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
		KeyPair keyPair = Keys.keyPairFor(SignatureAlgorithm.ES512);
		byte[] encoded = keyPair.getPublic().getEncoded();
		System.out.println(new String(encoded));
		Algorithm algorithm = Algorithm.ECDSA512((ECPublicKey) keyPair.getPublic(),
				(ECPrivateKey) keyPair.getPrivate());
		String token = JWT.create().withIssuer("auth0").withClaim("neat", true).sign(algorithm);
		System.out.println(token);

	}

}
