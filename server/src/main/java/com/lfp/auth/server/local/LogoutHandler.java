package com.lfp.auth.server.local;

import java.net.URI;
import java.util.function.Consumer;

import com.lfp.auth.auth0.management.config.Auth0ManagementConfig;
import com.lfp.auth.server.config.AuthServerConfig;
import com.lfp.auth.service.config.AuthServiceConfig;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.handler.MessageHandler;
import com.lfp.connect.undertow.handler.RedirectHandler;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.dns.DNSs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.PathHandler;

public class LogoutHandler implements HttpHandler, Consumer<PathHandler> {

	public static final LogoutHandler INSTANCE = new LogoutHandler();

	private final URI upstreamLogoutURI;

	public LogoutHandler() {
		upstreamLogoutURI = buildUpstreamLogoutURI();
	}

	@Override
	public void accept(PathHandler pathHandler) {
		var path = Configs.get(AuthServerConfig.class).logoutPath();
		UndertowUtils.appendPath(pathHandler, path, this);
	}

	@Override
	public void handleRequest(HttpServerExchange hse) throws Exception {
		if (this.upstreamLogoutURI == null) {
			new MessageHandler(StatusCodes.NOT_FOUND).handleRequest(hse);
			return;
		}
		var redirectURI = buildRedirectURI(hse);
		new RedirectHandler(redirectURI).handleRequest(hse);
	}

	private URI buildRedirectURI(HttpServerExchange hse) {
		var uri = this.upstreamLogoutURI;
		var returnToUri = getReturnToUri(hse);
		if (returnToUri == null)
			return uri;
		var urlb = UrlBuilder.fromUri(uri);
		urlb = urlb.addParameter(Configs.get(AuthServerConfig.class).upstreamLogoutReturnToParamName(),
				returnToUri.toString());
		return urlb.toUri();
	}

	private URI getReturnToUri(HttpServerExchange hse) {
		var uri = Configs.get(AuthServiceConfig.class).uri();
		if (uri == null)
			uri = URI.create(String.format("%s://%s", "http", DNSs.toWildcardDNSHostname("127.0.0.1")));
		var scheme = UndertowUtils.isSecure(hse) ? "https" : "http";
		if (!scheme.equalsIgnoreCase(uri.getScheme()))
			uri = UrlBuilder.fromUri(uri).withScheme(scheme).toUri();
		return uri;
	}

	private static URI buildUpstreamLogoutURI() {
		var auth0AdminConfig = Configs.get(Auth0ManagementConfig.class);
		var uri = auth0AdminConfig.uri();
		if (uri == null)
			return null;
		var authServerConfig = Configs.get(AuthServerConfig.class);
		var urlb = UrlBuilder.fromUri(uri);
		urlb = urlb.withPath(authServerConfig.upstreamLogoutPath());
		var clientId = auth0AdminConfig.clientId();
		if (Utils.Strings.isNotBlank(clientId))
			urlb = urlb.addParameter(auth0AdminConfig.clientIdKey(), clientId);
		return urlb.toUri();
	}

}
