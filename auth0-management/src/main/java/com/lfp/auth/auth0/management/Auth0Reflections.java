package com.lfp.auth.auth0.management;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.auth0.client.mgmt.ManagementAPI;
import com.auth0.net.BaseRequest;
import com.auth0.net.TelemetryInterceptor;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.utils.Utils;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

@SuppressWarnings("unchecked")
public class Auth0Reflections {

	private static final Field BaseRequest_client_FIELD;
	static {
		Field field = Utils.Functions.unchecked(() -> BaseRequest.class.getDeclaredField("client"));
		field.setAccessible(true);
		BaseRequest_client_FIELD = field;
	}
	private static final Method BaseRequest_createRequest_METHOD;
	static {
		Method method = Utils.Functions.unchecked(() -> BaseRequest.class.getDeclaredMethod("createRequest"));
		method.setAccessible(true);
		BaseRequest_createRequest_METHOD = method;
	}
	private static final Method BaseRequest_parseResponse_METHOD;
	static {
		Method method = Utils.Functions
				.unchecked(() -> BaseRequest.class.getDeclaredMethod("parseResponse", Response.class));
		method.setAccessible(true);
		BaseRequest_parseResponse_METHOD = method;
	}
	private static final FieldAccessor<ManagementAPI, TelemetryInterceptor> ManagementAPI_telemetry_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(ManagementAPI.class, true, "telemetry", TelemetryInterceptor.class);
	private static final FieldAccessor<ManagementAPI, OkHttpClient> ManagementAPI_client_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(ManagementAPI.class, true, "client", OkHttpClient.class);
	private static final FieldAccessor<ManagementAPI, Interceptor> ManagementAPI_logging_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(ManagementAPI.class, true, f -> {
				if (!"logging".equals(f.getName()))
					return false;
				if (!Interceptor.class.isAssignableFrom(f.getType()))
					return false;
				return true;
			});

	public static okhttp3.OkHttpClient getClient(BaseRequest<?> baseRequest) {
		return (okhttp3.OkHttpClient) Utils.Functions.unchecked(() -> BaseRequest_client_FIELD.get(baseRequest));
	}

	public static okhttp3.Request createRequest(BaseRequest<?> baseRequest) {
		return (okhttp3.Request) Utils.Functions.unchecked(() -> BaseRequest_createRequest_METHOD.invoke(baseRequest));
	}

	public static <T> T parseResponse(BaseRequest<T> baseRequest, Response response) {
		return (T) Utils.Functions.unchecked(() -> BaseRequest_parseResponse_METHOD.invoke(baseRequest, response));
	}

	public static TelemetryInterceptor getTelemetryInterceptor(ManagementAPI managementAPI) {
		return ManagementAPI_telemetry_FA.get(managementAPI);
	}

	public static Interceptor getLoggingInterceptor(ManagementAPI managementAPI) {
		return ManagementAPI_logging_FA.get(managementAPI);
	}

	public static OkHttpClient getOkHttpClient(ManagementAPI managementAPI) {
		return ManagementAPI_client_FA.get(managementAPI);
	}

	public static void settOkHttpClient(ManagementAPI managementAPI, OkHttpClient client) {
		ManagementAPI_client_FA.set(managementAPI, client);
	}
}
