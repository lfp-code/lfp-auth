package test;

import com.lfp.auth.auth0.auth.extension.Auth0GroupService;

public class GroupStreamTest {
	public static void main(String[] args) {
		var service = Auth0GroupService.INSTANCE;
		var groupStream = service.get();
		groupStream.forEach(v -> {
			var group = service.get(v.streamIds().findFirst().get()).values().findFirst().orElse(null);
			System.out.println(group);
		});
		System.err.println("done");
	}
}
