package com.lfp.auth.auth0.management;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.auth0.client.HttpOptions;
import com.auth0.client.ProxyOptions;
import com.auth0.client.mgmt.ActionsEntity;
import com.auth0.client.mgmt.BlacklistsEntity;
import com.auth0.client.mgmt.ClientGrantsEntity;
import com.auth0.client.mgmt.ClientsEntity;
import com.auth0.client.mgmt.ConnectionsEntity;
import com.auth0.client.mgmt.DeviceCredentialsEntity;
import com.auth0.client.mgmt.EmailProviderEntity;
import com.auth0.client.mgmt.EmailTemplatesEntity;
import com.auth0.client.mgmt.GrantsEntity;
import com.auth0.client.mgmt.GuardianEntity;
import com.auth0.client.mgmt.JobsEntity;
import com.auth0.client.mgmt.LogEventsEntity;
import com.auth0.client.mgmt.LogStreamsEntity;
import com.auth0.client.mgmt.ManagementAPI;
import com.auth0.client.mgmt.OrganizationsEntity;
import com.auth0.client.mgmt.ResourceServerEntity;
import com.auth0.client.mgmt.RolesEntity;
import com.auth0.client.mgmt.RulesConfigsEntity;
import com.auth0.client.mgmt.RulesEntity;
import com.auth0.client.mgmt.StatsEntity;
import com.auth0.client.mgmt.TenantsEntity;
import com.auth0.client.mgmt.TicketsEntity;
import com.auth0.client.mgmt.UserBlocksEntity;
import com.auth0.client.mgmt.UsersEntity;
import com.auth0.client.mgmt.filter.UserFilter;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.mgmt.users.User;
import com.auth0.net.BaseRequest;
import com.auth0.net.Request;
import com.auth0.net.Telemetry;
import com.auth0.net.TelemetryInterceptor;
import com.lfp.auth.auth0.management.config.Auth0ManagementConfig;
import com.lfp.data.redis.RedisConfig;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.concurrent.ImmutableRateLimiter.RateLimiterOptions;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.data.redisson.tools.concurrent.RateLimiter;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.lot.AbstractLot;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.Route;
import one.util.streamex.StreamEx;

public class ManagementAPILFP extends ManagementAPI {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	private static final int MAX_PER_PAGE = 100;

	public static final ManagementAPILFP INSTANCE = new ManagementAPILFP(
			Configs.get(Auth0ManagementConfig.class).uri().getHost());

	private final MemoizedSupplier<ManagementAPI> delegateSupplier = Utils.Functions.memoize(this::createDelegate);
	private final String domain;
	private final RateLimiter rateLimiter;

	private final HttpOptions httpOptions;

	protected ManagementAPILFP(String domain) {
		super(domain, Utils.Crypto.getRandomString());
		this.domain = Validate.notBlank(domain);
		var cfg = Configs.get(Auth0ManagementConfig.class);
		var redisConfig = Configs.get(RedisConfig.class);
		var urisFound = Utils.Lots.stream(redisConfig.tcpConnectionURIs()).nonNull().findFirst().isPresent();
		if (!urisFound)
			this.rateLimiter = RateLimiter.unlimited();
		else
			this.rateLimiter = RateLimiter.create(RateLimiterOptions
					.builder(cfg.rateLimiterRate(), cfg.rateLimiterInterval(), THIS_CLASS, VERSION, this.domain)
					.build());
		this.httpOptions = new HttpOptions();
	}

	public StreamEx<User> streamUsers() {
		return streamUsers(null);
	}

	public StreamEx<User> streamUsers(UserFilter userFilter) {
		var iter = new AbstractLot.Indexed<List<User>>() {

			private final Set<String> unique = new HashSet<>();

			@Override
			protected List<User> computeNext(long index) {
				var pageUserFilter = Optional.ofNullable(userFilter).orElse(new UserFilter());
				pageUserFilter = pageUserFilter.withPage((int) index, MAX_PER_PAGE);
				var userPage = Utils.Functions.unchecked(pageUserFilter, v -> {
					return users().list(v).execute();
				});
				var users = Utils.Lots.stream(userPage.getItems());
				users = users.nonNull();
				users = users.filter(v -> {
					if (Utils.Strings.isBlank(v.getId()))
						return false;
					return unique.add(v.getId());
				});
				var userList = users.toList();
				if (userList.isEmpty())
					return this.end();
				return userList;
			}
		};
		return Utils.Lots.flatMapIterables(Utils.Lots.stream(iter));
	}

	public ListenableFuture<User> modifyMetadata(String userId, Consumer<Map<String, Object>> modifier) {
		return modifyMetadata(userId, modifier, false);
	}

	public ListenableFuture<User> modifyMetadata(String userId, Consumer<Map<String, Object>> modifier,
			boolean disableSync) {
		requireUserId(userId);
		Objects.requireNonNull(modifier, "modifier required");
		ThrowingSupplier<ListenableFuture<User>, ?> updateFutureSupplier = () -> {
			var userFuture = executeAsync(ManagementAPILFP.INSTANCE.users().get(userId, null));
			var modUserFuture = userFuture.map(user -> {
				Objects.requireNonNull(user, () -> "user not found:" + userId);
				var userMetadata = Utils.Lots.stream(user.getUserMetadata()).filterKeys(Utils.Strings::isNotBlank)
						.toCustomMap(LinkedHashMap::new);
				modifier.accept(userMetadata);
				var modUser = new User();
				modUser.setUserMetadata(userMetadata);
				return modUser;
			});
			var updatedUserFuture = modUserFuture.flatMap(modUser -> {
				return executeAsync(ManagementAPILFP.INSTANCE.users().update(userId, modUser));
			});
			return updatedUserFuture;
		};
		ListenableFuture<User> result;
		if (disableSync)
			result = Threads.Futures.immediateFuture(updateFutureSupplier).flatMap(Function.identity());
		else {
			var semaphore = new KeepAliveSemaphore(RedissonClients.getDefault(), 1, THIS_CLASS, VERSION, userId,
					"metadata");
			if (MachineConfig.isDeveloper())
				semaphore.setVerboseLogging(true);
			result = semaphore.acquireAsyncKeepAliveFlatSupply(updateFutureSupplier);
		}
		return result.listener(() -> {
			if (MachineConfig.isDeveloper())
				logger.info("modifyMetadata future complete");
		});
	}

	public <T> ListenableFuture<T> executeAsync(Request<T> request) {
		Objects.requireNonNull(request);
		var sfuture = new SettableListenableFuture<T>(false);
		if (!(request instanceof BaseRequest)) {
			var cfuture = request.executeAsync();
			cfuture.whenComplete((v, t) -> {
				if (sfuture.isDone())
					return;
				Threads.Pools.centralPool().execute(() -> Threads.Futures.forwardCompletion(cfuture, sfuture));
			});
			sfuture.listener(() -> {
				if (cfuture.isDone())
					return;
				Threads.Pools.centralPool().execute(() -> Threads.Futures.forwardCompletion(sfuture, cfuture));
			});
			return sfuture;
		}
		BaseRequest<T> baseRequest = (BaseRequest<T>) request;
		okhttp3.Request okHttpRequest;
		try {
			okHttpRequest = Auth0Reflections.createRequest(baseRequest);
		} catch (Throwable t) {
			sfuture.setFailure(t);
			return sfuture;
		}
		var client = Auth0Reflections.getClient(baseRequest);
		var call = client.newCall(okHttpRequest);
		sfuture.listener(() -> {
			call.cancel();
		});
		call.enqueue(new okhttp3.Callback() {
			@Override
			public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
				Threads.Pools.centralPool().execute(() -> {
					sfuture.setFailure(new Auth0Exception("Failed to execute request", e));
				});
			}

			@Override
			public void onResponse(@NotNull okhttp3.Call call, @NotNull Response response) throws IOException {
				try {
					T parsedResponse = Auth0Reflections.parseResponse(baseRequest, response);
					Threads.Pools.centralPool().execute(() -> sfuture.setResult(parsedResponse));
				} catch (Throwable t) {
					Threads.Pools.centralPool().execute(() -> sfuture.setFailure(t));
				}
			}
		});
		return sfuture;

	}

	protected String getToken() {
		return Utils.Functions.unchecked(() -> AuthAdminTokenSupplier.INSTANCE.get()).getAccessToken();
	}

	protected ManagementAPI createDelegate() {
		var delegate = new ManagementAPI(domain, getToken());
		var telemetry = Auth0Reflections.getTelemetryInterceptor(delegate);
		var logging = Auth0Reflections.getLoggingInterceptor(delegate);
		var client = buildNetworkingClient(this.httpOptions, telemetry, logging);
		Auth0Reflections.settOkHttpClient(delegate, client);
		return delegate;
	}

	protected OkHttpClient buildNetworkingClient(HttpOptions options, TelemetryInterceptor telemetryInterceptor,
			Interceptor loggingInterceptor) {
		OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
		final ProxyOptions proxyOptions = options.getProxyOptions();
		if (proxyOptions != null) {
			// Set proxy
			clientBuilder.proxy(proxyOptions.getProxy());
			// Set authentication, if present
			final String proxyAuth = proxyOptions.getBasicAuthentication();
			if (proxyAuth != null) {
				clientBuilder.proxyAuthenticator(new Authenticator() {

					@Override
					public okhttp3.Request authenticate(Route route, Response response) throws IOException {
						if (response.request().header(Headers.PROXY_AUTHORIZATION) != null)
							return null;
						return response.request().newBuilder().header(Headers.PROXY_AUTHORIZATION, proxyAuth).build();
					}
				});
			}
		}
		return clientBuilder.addInterceptor(loggingInterceptor).addInterceptor(telemetryInterceptor)
				.addInterceptor(createRateLimiterInterceptor())
				.connectTimeout(options.getConnectTimeout(), TimeUnit.SECONDS)
				.readTimeout(options.getReadTimeout(), TimeUnit.SECONDS).build();
	}

	protected Interceptor createRateLimiterInterceptor() {
		return chain -> {
			var request = chain.request();
			var host = request.url().host();
			if (Utils.Strings.equalsIgnoreCase(host, this.domain)
					|| Utils.Strings.endsWithIgnoreCase(host, "." + this.domain))
				Throws.unchecked(rateLimiter::acquire);
			return chain.proceed(request);

		};
	}

	protected ManagementAPI getDelegate() {
		var delegate = delegateSupplier.get();
		delegate.setApiToken(getToken());
		return delegate;
	}

	private static void requireUserId(String userId) {
		Validate.notBlank(userId, "invalid user id:%s", userId);
	}

	// delegates

	@Override
	public int hashCode() {
		return getDelegate().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return getDelegate().equals(obj);
	}

	@Override
	public void setApiToken(String apiToken) {
		getDelegate().setApiToken(apiToken);
	}

	@Override
	public void doNotSendTelemetry() {
		getDelegate().doNotSendTelemetry();
	}

	@Override
	public void setTelemetry(Telemetry telemetry) {
		getDelegate().setTelemetry(telemetry);
	}

	@Override
	public void setLoggingEnabled(boolean enabled) {
		getDelegate().setLoggingEnabled(enabled);
	}

	@Override
	public ClientGrantsEntity clientGrants() {
		return getDelegate().clientGrants();
	}

	@Override
	public ClientsEntity clients() {
		return getDelegate().clients();
	}

	@Override
	public ConnectionsEntity connections() {
		return getDelegate().connections();
	}

	@Override
	public DeviceCredentialsEntity deviceCredentials() {
		return getDelegate().deviceCredentials();
	}

	@Override
	public GrantsEntity grants() {
		return getDelegate().grants();
	}

	@Override
	public LogEventsEntity logEvents() {
		return getDelegate().logEvents();
	}

	@Override
	public LogStreamsEntity logStreams() {
		return getDelegate().logStreams();
	}

	@Override
	public RulesEntity rules() {
		return getDelegate().rules();
	}

	@Override
	public RulesConfigsEntity rulesConfigs() {
		return getDelegate().rulesConfigs();
	}

	@Override
	public UserBlocksEntity userBlocks() {
		return getDelegate().userBlocks();
	}

	@Override
	public UsersEntity users() {
		return getDelegate().users();
	}

	@Override
	public BlacklistsEntity blacklists() {
		return getDelegate().blacklists();
	}

	@Override
	public EmailTemplatesEntity emailTemplates() {
		return getDelegate().emailTemplates();
	}

	@Override
	public EmailProviderEntity emailProvider() {
		return getDelegate().emailProvider();
	}

	@Override
	public GuardianEntity guardian() {
		return getDelegate().guardian();
	}

	@Override
	public StatsEntity stats() {
		return getDelegate().stats();
	}

	@Override
	public TenantsEntity tenants() {
		return getDelegate().tenants();
	}

	@Override
	public TicketsEntity tickets() {
		return getDelegate().tickets();
	}

	@Override
	public String toString() {
		return getDelegate().toString();
	}

	@Override
	public ResourceServerEntity resourceServers() {
		return getDelegate().resourceServers();
	}

	@Override
	public JobsEntity jobs() {
		return getDelegate().jobs();
	}

	@Override
	public RolesEntity roles() {
		return getDelegate().roles();
	}

	@Override
	public OrganizationsEntity organizations() {
		return getDelegate().organizations();
	}

	@Override
	public ActionsEntity actions() {
		return getDelegate().actions();
	}

}