package com.lfp.auth.impl;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.lfp.auth.service.read.GroupMemberService;
import com.lfp.auth.service.read.UserMembershipService;
import com.lfp.auth.service.types.GroupMembers;
import com.lfp.auth.service.types.HasIds;
import com.lfp.auth.service.types.UserMemberships;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.CachedKeyGetter;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public interface DelegatingGroupMemberService extends GroupMemberService {

	UserMembershipService getDelegate();

	@Override
	default StreamEx<GroupMembers> get() {
		var delegate = getDelegate();
		if (delegate == null)
			return StreamEx.empty();
		return getGroupMembers(delegate.get(), null);
	}

	@Override
	default EntryStream<Entry<String, Boolean>, GroupMembers> get(Iterable<? extends Entry<String, Boolean>> keyIble) {
		var delegate = getDelegate();
		if (delegate == null)
			return EntryStream.empty();
		if (keyIble == null)
			keyIble = List.of();
		StreamEx<GroupMembers> groupMembersStream = getGroupMembers(delegate.get(), keyIble);
		var estreams = groupMembersStream.map(gm -> {
			return HasIds.streamIds(gm).mapToEntry(nil -> gm.isCalculate()).map(ent -> ent).mapToEntry(nil -> gm);
		});
		return estreams.chain(Utils.Lots::flatMap).chain(Utils.Lots::streamEntries);
	}

	@SuppressWarnings("resource")
	private static StreamEx<GroupMembers> getGroupMembers(Iterable<? extends UserMemberships> userMemberships,
			Iterable<? extends Entry<String, Boolean>> keyIble) {
		if (userMemberships == null)
			return StreamEx.empty();
		CachedKeyGetter<Entry<String, Boolean>, Entry<String, Boolean>, Boolean> cachedKeyGetter;
		if (keyIble != null) {
			Entry<Boolean, StreamEx<Entry<String, Boolean>>> hasNextEntry = Utils.Lots
					.hasNext(AuthImplUtils.normalizeEntryStream(keyIble));
			if (!hasNextEntry.getKey())
				return StreamEx.empty();
			cachedKeyGetter = Utils.Functions.newCachedKeyGetter(hasNextEntry.getValue(), ent -> Arrays.asList(ent),
					ent -> Arrays.asList(true));
		} else
			cachedKeyGetter = null;
		Map<Entry<String, Boolean>, Set<String>> groupIdCalcToUserId = new ConcurrentHashMap<>();
		for (var um : AuthImplUtils.normalizeStream(userMemberships)) {
			for (var groupId : um.streamGroupIds()) {
				var key = Utils.Lots.entry(groupId, um.isCalculate());
				if (cachedKeyGetter != null) {
					var exists = cachedKeyGetter.apply(key).orElse(false);
					if (!exists)
						continue;
				}
				groupIdCalcToUserId.computeIfAbsent(key, nil -> new LinkedHashSet<>()).add(um.getUserId());
			}
		}
		return Utils.Lots.stream(groupIdCalcToUserId).map(ent -> {
			var groupIdCalc = ent.getKey();
			var userIds = ent.getValue();
			GroupMembers.Builder gmb = GroupMembers.builder();
			gmb.beanId(groupIdCalc.getKey());
			gmb.calculate(groupIdCalc.getValue());
			gmb.values(List.copyOf(userIds));
			return gmb.build();
		});
	}
}
